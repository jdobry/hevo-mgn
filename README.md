Info
=======================
* Project homepage  https://www.thingiverse.com/thing:4663434
* [Gallery of full resolution photos](https://photos.app.goo.gl/GqucPuGycDGMSorR7)
* [design file in SCAD](https://gitlab.com/jdobry/hevo-mgn/-/tree/v3/SCAD)
* [Alternate sources of STLs](https://gitlab.com/jdobry/hevo-mgn/-/tree/v3/STLv3)
* [How-to print notices](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/DOC/print.md)
* [Here is sources of this remix](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/DOC/remixSources.md). I am not able to add "remix" source into thingiverse, it is broken now.
* Don't ask me for STEP format. A make this [design in SCAD](https://gitlab.com/jdobry/hevo-mgn/-/tree/v3/SCAD) and conversion to STEP is NOT possible
* Don't ask me for modifications. I publish all resources and you can modify it personally, or use it as it is.
* Enjoy this printer and DONT forget to share your experience and/or modifications!
* Most of links on BOM list are affiliate links. Please use it, it is sufficient reward for me.
* [Note about compatibility](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/DOC/compatibility.md) with [HEVO-MGN v2 (Hypercube evolution with MGN linear rails)](https://www.thingiverse.com/thing:4186819)
* [todo list](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/DOC/todo.md)


Notes to OpenSCAD source:

Needed libs:
* https://www.thingiverse.com/thing:1208001
* https://www.thingiverse.com/thing:2784911
* https://grabcad.com/library/opto-endstop-tcst2103-1
* https://www.thingiverse.com/thing:1576438

Titan mockup:

There are two options for Titan model
* Full detail model - Convert http://files.e3d-online.com/Titan/ASM_EX_AERO_175.stp to titan_assembly_simple.stl file
* simplified model - find "module titan()" and change "stl = true;" to "stl = false;"

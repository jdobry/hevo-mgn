Main features
=======================
* [Mellow NF-BMG-WIND V6 Dual Drive BMG Extruder - "right" orientation](https://s.click.aliexpress.com/e/_9Hgnf7)
* X axis - [MGN15 rail with MGN15H block](https://www.aliexpress.com/item/32600000975.html) for X axis. Length is your X 3030 profile length + 56mm (for standard 300x300x300 it is 476mm)
* Y axis -  [2pcs MGN12 rail with MGN12H block](https://www.aliexpress.com/item/32829826159.html) for Y axis. Lenght is same as your Y 3030 profile ((for standard 300x300x300 it is 410mm)
* direct connection between X axis rail and Y carriage to eliminate flexibility of printed parts
* Reworked Z axis setup. 4* 12mm rods + 4*[LM12LUU bearings](https://s.click.aliexpress.com/e/_9fKaqL)
* Frame profiles 3030 and bed frame profiles 2020 is same as original HEVO
** 4* [3030 profile](https://www.motedis.co.uk/shop/Slot-profiles/Profile-30-B-Type-slot-8/Profile-30x30-B-Type-Slot-8::99999432.html) on X axis 420mm
** 6* [3030 profile](https://www.motedis.co.uk/shop/Slot-profiles/Profile-30-B-Type-slot-8/Profile-30x30-B-Type-Slot-8::99999432.html) on Y axis 410mm
** 4* [3030 profile](https://www.motedis.co.uk/shop/Slot-profiles/Profile-30-B-Type-slot-8/Profile-30x30-B-Type-Slot-8::99999432.html) on Y axis 500mm
** 2* [2020 profile](https://www.motedis.co.uk/shop/Slot-profiles/Profile-20-B-Type-slot-6/Profile-20x20-B-type-slot-6::999991.html) on bed frame X 415mm
** 2* or 3* [2020 profile](https://www.motedis.co.uk/shop/Slot-profiles/Profile-20-B-Type-slot-6/Profile-20x20-B-type-slot-6::999991.html) on bed frame Y 240mm
** 4* [rod Ø12 mm](https://www.motedis.co.uk/shop/Dynamics-linear-units/precision-shafts/Precision-shaft-steel/Precision-steel-shaft-%C3%9812mm::999996496.html) 400mm

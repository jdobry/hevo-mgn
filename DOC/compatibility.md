This v3 design is compatible with [HEVO-MGN v2 (Hypercube evolution with MGN linear rails)](https://www.thingiverse.com/thing:4186819) with this notices
==============================================
* head setup with [Mellow NF-BMG-WIND V6 Dual Drive BMG Extruder - "right" orientation](https://s.click.aliexpress.com/e/_9Hgnf7) is compatible with original Z axis setup used on v2, but you will need [longer X endstop](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/STLv3/CoreXY/p_xFlag_extended_25mm.stl) flag to protect fan duct ring
* Z axis setup from v3 can be used on v2 without changes.
* CoreXY printed parts from v2 can be used on v3 except head.
* Head setup from v2 (Titan extruder) can be used on v3 without changes
* Z setup parts can be used from original HEVO but with [longer X endstop](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/STLv3/CoreXY/p_xFlag_extended_25mm.stl) flag to protect fan duct ring or with upper parts from this my design.

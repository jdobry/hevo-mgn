Print notes
=============
Print it with this parametres in this setting with exception bellow.
* PET-G
* nozzle 0.4 mm
* layer 0.2 mm
* 4 or 5 perimetres
* 6 bottom layers, 7 top layers
* 40% infill
* no support needed

Exceptions
-----------
[head/p_fanConnB_f.stl](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/STLv3/head/p_fanConnB_f.stl)
* need support. I am prepare project file for Prusa slicer [head/p_fanConnB_f.3mf](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/STLv3/head/p_fanConnB_f.3mf)

[head/p_front.stl](https://gitlab.com/jdobry/hevo-mgn/-/blob/v3/STLv3/head/p_front.stl)
* high temperature material needed. ABS or CPE etc.
* use supports

BOM (bill of material)
=======================
Head
-----------------------
* [Mellow NF-BMG-WIND V6 Dual Drive BMG Extruder - "right" orientation](https://s.click.aliexpress.com/e/_9Hgnf7)
* [high quality M3x5x5 brass insert](https://s.click.aliexpress.com/e/_98mUWH) to plug into plastics. Don't ask me for the nut version of printed parts. Brass inserts are far better than nuts, specially in PETG. [Or tri budget variant M3x5x5 brass insers](http://s.click.aliexpress.com/e/b6tSyWco)
* [high quality M3x5x7 brass insert](https://s.click.aliexpress.com/e/_98mUWH) or you can use M3x5x5 instead, but longer are better.
* [M3x4x3 brass inserts](https://s.click.aliexpress.com/e/_dWrKRTR) It is needed for BLtouch mount and Xmin sensor.
* [M2x3.2x3 brass insert](https://s.click.aliexpress.com/e/_AbQkT8) 
* [BL touch](http://s.click.aliexpress.com/e/ev82xZq). I recommend original here. I was try few clones, it was lost money due to instability of measurement.

CoreXY
-----------------------
* [MGN15 rail with MGN15H block](https://www.aliexpress.com/item/32600000975.html) for X axis. Length is your X 3030 profile length + 56mm (for standard 300x300x300 it is 476mm)
* [2pcs MGN12 rail with MGN12H block](https://www.aliexpress.com/item/32829826159.html) for Y axis. Lenght is same as your Y 3030 profile ((for standard 300x300x300 it is 410mm)
* I recommend this [high quality GT2 pulles](https://s.click.aliexpress.com/e/_d6QUMDq). 2pcs without teeth 6pcs  with teeth. Far better than china standart, but with 10mm width, this design accept this one or standart chep one.
* 2pcs [GT2 Belt Pulley 20 teeth](https://s.click.aliexpress.com/e/_dZ8KaFa)
* [M3 hammer nuts for 3030 profile](http://s.click.aliexpress.com/e/nG4Md9W) to mount MGN12 Y rails

Frame
-----------------------
* [4pcs 3-way corner for 3030 profile.](http://s.click.aliexpress.com/e/bZdNrOh2). It is needed, because my motor holders and idler holders are not compatible with inner "L" corners. 

Electronics
-----------------------
* [BIGTREETECH SKR PRO V1.2 Controller Board 32 Bit](https://s.click.aliexpress.com/e/_9v6wUv)
* [4pcs TMC2130 With SPI](https://s.click.aliexpress.com/e/_9iVnvX ) or [4pcs TMC2209 V1.2 Stepper Motor Driver](https://s.click.aliexpress.com/e/_AbH5Tj) 
* [SKR PRO 1.1/1.2 holder with 40mm fan](https://www.thingiverse.com/thing:4274848)

Other
------------------------
* lot of DIN912  M3 stainless screws. I can recommend [this set](https://www.aliexpress.com/item/32815535720.html), but need more screws. For ex cca 16pcs M3x12 to mount Y rails.
* lot of DIN912 M3 stainless screws. I can recommend [this set](https://s.click.aliexpress.com/e/_A2VMkB)
* [nema17 stepper motor with T8 screw lead 2mm 400mm](https://s.click.aliexpress.com/e/_A1zYuD) and cut it to 350mm.

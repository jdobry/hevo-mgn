$fn=50;

hgt = 7;
fingerDia = 5.5;

//M4
 nutHeight = 3;
 nutDia = 7 / cos(30);
 hole = 4.4;
 thumb = 10;
 dia = 25; 

//M3
// nutHeight = 3;
// nutDia = 5.5 / cos(30);
// hole = 3.4;
// thumb = 8;
// dia = 23; 

steps = 10;
pathRadius = dia/2+1;

module blNut()
{
  difference([]) {
    union() {
      translate([0,0,2]) cylinder(d=dia,h=hgt-4);
      cylinder(d2=dia,d1=dia-1.5,h=2);
      translate([0,0,hgt-2]) cylinder(d1=dia,d2=dia-1.5,h=2);
      cylinder(d1=thumb + 6,d2=thumb, h=hgt+10);
      cylinder(d=thumb,h=hgt+18);
    }
      for (i=[1:steps])  {
          translate([pathRadius*cos(i*(360/steps)),pathRadius*sin(i*(360/steps)),-1]) {
                  cylinder(d=fingerDia,h=hgt+2);
          }
      }
      translate([0,0,nutHeight+hgt-0.8]) cylinder(d=hole,h=50);
      translate([0,0,-1])
          cylinder(d1=nutDia+0.6, d2=nutDia-0.1, h=nutHeight + hgt, $fn=6);
      translate([0,0,-1])
          cylinder(d1=nutDia+3, d2=nutDia/1.2, h=hgt*0.7, $fn=60);
      //translate([0,0,-1]) cube(100);
  }
}

blNut();
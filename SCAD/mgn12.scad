//$fn=64;


module mgn12rail(len, hOffset)
{
  difference()
  {
    translate([0,-12/2,0])
      cube([len,12,8]);
    x = len/25;
    for(i = [0:x])
    {
      translate([i*25 + hOffset,0,-1])
        cylinder(d=3.5,h=12);
      translate([i*25 + hOffset,0,10-4.5])
        cylinder(d=6,h=6);
    }
    translate([-1,12/2, 8-3])
      rotate([0,90,0])
        cylinder (d=3,h=len+2,$fn=8);
    translate([-1,-12/2, 8-3])
      rotate([0,90,0])
        cylinder (d=3,h=len+2,$fn=8);
  }
}

module mgn12h()
{
  translate([0,0,3])
  difference()
  {
    translate([-45.4/2, -27/2, 0])
      cube([45.4,27,13-3]);
    translate([-30,0,-3])
      mgn12rail(60,100);
    translate([10,10,(13-3)-4]) 
      cylinder(d=3,h=4+1);
    translate([-10,10,(13-3)-4]) 
      cylinder(d=3,h=4+1);
    translate([10,-10,(13-3)-4]) 
      cylinder(d=3,h=4+1);
    translate([-10,-10,(13-3)-4]) 
      cylinder(d=3,h=4+1);
  }
}

color("silver") render() mgn12rail(470,15);
color("green") render() translate([50,0,0]) mgn12h();
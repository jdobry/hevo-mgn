//$fn=64;


module mgn15rail(len, hOffset)
{
  difference()
  {
    translate([0,-15/2,0])
      cube([len,15,10]);
    x = len/40;
    for(i = [0:x])
    {
      translate([i*40 + hOffset,0,-1])
        cylinder(d=3.5,h=12);
      translate([i*40 + hOffset,0,10-4.5])
        cylinder(d=6,h=6);
    }
    translate([-1,15/2, 10-3])
      rotate([0,90,0])
        cylinder (d=3,h=len+2);
    translate([-1,-15/2, 10-3])
      rotate([0,90,0])
        cylinder (d=3,h=len+2);
  }
}

module mgn15h()
{
  translate([0,0,4])
  difference()
  {
    translate([-58.8/2, -32/2, 0])
      cube([58.8,32,16-4-0.01]);
    translate([-30,0,-4])
      mgn15rail(60,100);
    translate([12.5,12.5,(16-4)-4]) 
      cylinder(d=3,h=4+1);
    translate([-12.5,12.5,(16-4)-4]) 
      cylinder(d=3,h=4+1);
    translate([12.5,-12.5,(16-4)-4]) 
      cylinder(d=3,h=4+1);
    translate([-12.5,-12.5,(16-4)-4]) 
      cylinder(d=3,h=4+1);
  }
}

color("silver") render() mgn15rail(470,15);
color("green") render() translate([50,0,0]) mgn15h();
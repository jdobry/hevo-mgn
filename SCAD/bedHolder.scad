
$fn = $preview ? 0 : 128;

distance = 15;
offset = 13; /* 0 ~ 20 */

M5hole = 5.5;
M5head = 10;
M4hole = 4.4;
M4head = 10.5;

module bedHolder(dist,add=0)
{
    yOnProfile = 4;// 16.5;
  difference()
  {
    union()
    {
      plus = 18;
      hull()
      {
        translate([0,-dist,-plus])
          cylinder(d=13,h=plus+5+add);
        translate([-10-offset,-5,-plus])
          cube([20+offset,yOnProfile+5,plus+5+add]);
          
      }
      translate([-offset-10,-5,-plus])
        cube([offset+10,yOnProfile+5,5+plus+add]);
    }
    translate([-80/2,0,-80])
      cube([80,80,80]);
    translate([0,1,-10])
      rotate([90,0,0])
        cylinder(d=M5hole,h=30);
    translate([-offset,1,-10])
      rotate([90,0,0])
        cylinder(d=M5hole,h=30);
    translate([-offset,-5,-10])
        rotate([90,0,0])
          cylinder(d=M5head,h=30);
    hull()
    {
      translate([0,-5,-10])
        rotate([90,0,0])
          cylinder(d=M5head,h=30);
      translate([0,-5,-30])
        rotate([90,0,0])
          cylinder(d=M5head,h=30);
      translate([0,-dist,-5-20])
        cylinder(d=M4head,h=20);  
    }
    hull()
    
    translate([0,-dist,-30])
      cylinder(d=M4hole,h=40+add);    
    *translate([0,-dist,-5-20])
      cylinder(d=M4head,h=20);  
    hull()
    {
      *translate([0,-dist,-5])
        cylinder(d=M4head,h=0.01);  
      translate([-40,-5-(dist+20),-18-10])
        cube([80,dist+20,10]);
      translate([-40,-dist-(dist+20),-5-10])
        cube([80,dist+20,10]);
    }
  }
}


if (!$preview)
{
  rotate([180,0,0])
  {
    translate([-12,0,0]) bedHolder(distance);
    translate([12,0,0]) mirror([1,0,0]) bedHolder(distance);
  }
}
else
{
  translate([50,0,0]) bedHolder(10);
  translate([100,0,0]) bedHolder(15);
  translate([150,0,0]) bedHolder(20);
  translate([200,0,0]) bedHolder(25);
  translate([250,0,0]) bedHolder(30);
  translate([300,0,0]) bedHolder(35);
  translate([350,0,0]) bedHolder(40);
  translate([400,0,0]) bedHolder(45);

  color("silver", 0.4)
    translate([-40,0.01,-20.01]) 
      cube([80,20,20]);
}


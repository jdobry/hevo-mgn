
use <mgn15.scad>
$fn = 64;

/* [magnet] */
magnetDia = 12.4;
magnetThin = 5;

/* [dial indicator distance] */
distance = 20;

nutDia = 10.4 / cos(30);

M3insertD = 4.7;

module dialHole() {
  hull()
  {
    translate([-5.2/2,-14,-16.4/2]) cube([5.2,14,16.4]);
    rotate([0,-90,0])
      translate([0,0,-5.2/2]) 
        cylinder(d=16.4,h=5.2);
  }
  hl = (20-6-0.2)*2;
  rotate([0,90,0])
    translate([0,0,-hl/2]) 
      cylinder(d=6.4,h=hl);
  translate([0,-14,0]) rotate([-90,0,0]) cylinder(d=18.2,h=3);
  translate([0,-14,0]) rotate([-90,0,0]) cylinder(r=26,h=2);
  rotate([0,90,0])
    translate([0,0,20-6]) 
      cylinder(d=nutDia,h=10,$fn=6);
}


module holder()
{
  difference()
  {
    union()
    {
      difference()
      {
        translate([-40/2,-23+10,-40+15-distance+15]) cube([40,21,38+distance-15]);
        translate([-50/2,-15/2,0]) cube([50,25,40]);
        translate([-40/2+magnetDia/2+2,0.5,-magnetThin]) 
          cylinder(d=magnetDia,h=magnetThin+1);
        translate([+40/2-magnetDia/2-2,0.5,-magnetThin]) 
          cylinder(d=magnetDia,h=magnetThin+1);
        translate([-40/2+magnetDia/2+2,-magnetThin-15/2,10/2]) 
          rotate([-90,0,0])
            cylinder(d=magnetDia,h=magnetThin+10);
        translate([40/2-magnetDia/2-2,-magnetThin-15/2,10/2]) 
          rotate([-90,0,0])
            cylinder(d=magnetDia,h=magnetThin+10);
        translate([-50-5/2,-50/2,-50-10]) cube(50);
        translate([0,-1.2,-distance]) dialHole();
      }
      //translate([0,0,-7-3-0.01]) cylinder(d=5+4,h=7+3);
    }
    translate([0,0,-6]) cylinder(d=M3insertD,h=7);
  }
}

if ($preview)
{
  holder();
  color("silver",0.4) 
    translate([-100/2,0,0]) 
      mgn15rail(100,10);
}
else
{
  rotate([0,90,0])
    holder();
}


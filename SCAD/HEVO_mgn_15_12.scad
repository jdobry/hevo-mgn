/*
 * Project home
 * v2 https://www.thingiverse.com/thing:4186819
 * v3 https://www.thingiverse.com/thing:4663434
 */

use <splines.scad> //https://www.thingiverse.com/thing:1208001
use <Naca_sweep.scad> //https://www.thingiverse.com/thing:1208001 

//use <XY_carriage.scad>
use <cooler.scad>
//use <sg90mockup.scad>
//use <irSensor.scad>
use <mgn15.scad>
use <mgn12.scad>

use <jet.scad>

use <Fan_parametric_v1.scad>

use <Zsetup.scad>

use <bedHolder.scad>

use <bed_leveling_nut.scad>

//
//use <customizable_fan_v1.scad> // https://www.thingiverse.com/thing:2374869

//$fn=32;

//teardown=25;
//teardown=0.01;
teardown=0;
cornerTest = false; // test header on all conrnes in one moment
//cornerTest = true;
details = (cornerTest) ? 10 : 10;
//details = 0;

show = true;

lightFlags=false;

BMG_Aero = 1; // false;
//BMG_Aero = true;

rightAddon = 0;

// cheap china idler
//idlerDia = 12.1;
//idlerDia2 = 18;
//idlerPitch = 12.7;
//idlerH  = 9;
//idlerHw  = 1;
//idlerHoleDia = 20;

// high quality Mellow idler
//https://s.click.aliexpress.com/e/_dWdFyRn
idlerDia = 12;
idlerDia2 = 15;
idlerPitch = 12.7;
idlerH  = 10;
idlerHw  = 1.5;
idlerHoleDia = 20;

bandW = 6; // GT2 x 6mm

coolerY = 11;

//XflagLen = 7;   // normal Xflag
XflagLen = 7 +  3; // extended Xflag


M2hole = 2.5;
M2insertD = 2.8;

M3insertD = 4.7;
M3insertH = 5.5;
M3insertD_small = 3.6;  // https://www.aliexpress.com/item/32810928309.html
M3insertD_bltouch = M3insertD_small;
M3insertD_Xsensor = M3insertD_small;
M3hole = 3.4;
M3head = 6.2;
M3thread = 2.5; // screw directly to plastics

M5hole = 5.5;
M5head = 11.5;

bandAxisDevianceX = 15-42.3/2;
bandAxisX = 450/2 + bandAxisDevianceX;

mgnPos = 60/2 + 3 + 16 - 10;
frameOffsetZ = mgnPos+teardown+30;
mgnYposZ = frameOffsetZ;
mgnXposZ = frameOffsetZ-10-13;
idler2idlerPitch = 2;
idler2posZ = mgnXposZ-idlerH/2-5;     //upper
idler1posZ = idler2posZ-idlerH-idler2idlerPitch;   //lower
bandMiddleZ = (idler1posZ + idler2posZ) / 2;
xyMotor1PosZ = idler1posZ-12;
xyMotor2PosZ = idler2posZ-12;

headX_L = -150+1+5;
headX_R =  157;
headY_F = -143.5;
headY_R =  163;
bedTop = -8;
bedBottom = -308;

bedYoffset = -30;
bedXoffset = -5;

headX = 0;//-150;//157; //-100; //-150;  //-155 ~ 157
headY = 0;//164;//-145; //0;//-145;  //-145 ~ 164

echo ("XYZ size", [headX_R - headX_L, headY_R - headY_F, bedTop - bedBottom]);

//headY = 163;
conn = "pipe"; // or "chain";

mgnPlusY = -9; //15/2;

pipeD = 16.5; //8

motorWall = 1.5;

showpipe = ((conn == "pipe") && (headX==0) && (headY==0) && !cornerTest && (details>9))
  ? true : false;
  

//$fn=64;

//rodOffset = 16/2+5;
//motorBodyLen = 30;

motorBodyLen = 26; // E3D Slimline https://e3d-online.com/nema17-slimline-stepper-motor
//motorBodyLen = 30;
rodOffset = motorBodyLen/2-3/2+5;

titan2bmgDiff = [-7.15,-10.2,1.8];

bltouchPosTitan = [10+2-3,-4+7.5,14+1];
bltouchPosBMGaero = bltouchPosTitan;
titan2bmgDiffRot = [titan2bmgDiff[1],-titan2bmgDiff[0],titan2bmgDiff[2]];
Cdiff = (BMG_Aero) ? titan2bmgDiffRot : [0,0,0];

bltouchPos = (BMG_Aero) ? bltouchPosBMGaero : bltouchPosTitan;

blTouchTouch = 40.3 + 4.3; // https://docs.wixstatic.com/ugd/f5a1c8_d40d077cf5c24918bd25b6524f649f11.pdf

Yrail = (details >= 1) ? 476 : 120;

nozzlePosBMGaero = [-17.2,-38.85,-24.8-4];
nozzlePosTitan = [-7,-46,-29.60-2];

motorFiletD = 22.5;

motorAxisZ = (BMG_Aero) ? 11 : 13;


headXoffset = 26;
carTranformation = [-(headXoffset-rodOffset),-(-35),-(motorAxisZ)];

carOffset = 0;

carConnOffset = [carOffset,-1+9,bandMiddleZ];
carConn = carTranformation + carConnOffset;

carryX = 48+2;
carryZ = 48-6 + (15 - motorAxisZ);

frontBackConn = -(-(-35)-1+9)+carryX/2;

motor2absTransform = [headXoffset-rodOffset,-35,motorAxisZ];
fanscrew_1_mot = [-23,31,-4];
fanscrew_2_mot = fanscrew_1_mot + [0,10,0];

fanscrew_1_abs = fanscrew_1_mot + motor2absTransform;
fanscrew_2_abs = fanscrew_2_mot + motor2absTransform;

fanRotate = 35;

bltouchPos_mot = bltouchPos - motor2absTransform;

cableHolder2 = false; // another cable holder on head, unused 

fanWall = 1.7;

fanCutAngle = 50;

showFan = 1;
showBLtouch = showFan;
showExtruder = showFan;

z3in1 = 1;

module nema17(len=motorBodyLen, axisL=24)
{
  difference()
  {
    intersection()
    {
      translate([-42.3/2,-42.3/2,-len+0.01]) cube ([42.3,42.3,len-0.02]);
      
      rotate([0,0,45]) translate([-54/2,-54/2,-len]) cube ([54,54,len]);
    }
    translate([31/2,31/2,-9]) cylinder(d=3,h=10);
    translate([-31/2,31/2,-9]) cylinder(d=3,h=10);
    translate([31/2,-31/2,-9]) cylinder(d=3,h=10);
    translate([-31/2,-31/2,-9]) cylinder(d=3,h=10);
  }
  cylinder(d=22,h=2);
  cylinder(d=5,h=axisL);
  
}

module titanHOB()
{
  translate([0,0,20.7]) mirror([0,0,1])
  difference()
  {
    union () {
      cylinder(d=5, h=20.7);
      translate([0,0,13.1]) cylinder(d=34, h=4);  
      translate([0,0,2]) cylinder(d=8, h=17.7);
    }  
    translate([0,0,-1]) cylinder(d=3,h=30);
  }
}

module hotend(rot = 0)
{
  translate([-20.7,-11,13.5]) rotate([0,-90,0]) 
  { 
    cylinder(d=6,h=22);
    translate([0,0,22-3]) cylinder(d=7*(sin(30)+1),h=3, $fn=6);
    translate([0,0,22]) cylinder(d1=4, d2=1 ,h=2);
    rotate([0,0,rot]) translate([-4.5-1.5,-8-1.5,6]) cube([20+3,16+3,11.5+4]);
  }
}

module titan()
{
  stl = true;
  if (stl)
  {
    // converted from http://files.e3d-online.com/Titan/ASM_EX_AERO_175.stp
    rotate([0,0,90]) translate([6.7,22.9,0]) rotate([0,180,0]) 
      //import ("titan_assembly_supersimple.stl"); 
      import ("titan_assembly_simple.stl"); 

    translate([31/2,-31/2,(25.4-20.7)/2])  titanHOB();
    //translate([23,21,12]) rotate([-90,0,0]) cylinder(d=18,h=10);
  }  
  else
  {
    difference()
    {
      translate([-42.3/2,-(45.5-42.3/2),0]) cube([43.5, 45.5, 25.4]);
      translate([-42.3/2+2,-(45.5-42.3/2)-2,2]) cube([43.5, 43.5, 25.4-4]);
      translate([31/2,31/2,-1]) cylinder(d=3,h=30);
      translate([-31/2,31/2,-1]) cylinder(d=3,h=30);
      translate([31/2,-31/2,-1]) cylinder(d=3,h=30);
      translate([-31/2,-31/2,-1]) cylinder(d=3,h=30);
      translate([0,0,-0.1]) cylinder(d=16,h=3);
    }
  }
  hotend(90);
  translate([31/2,-31/2,(25.4-20.7)/2])  titanHOB();
  //translate([23,21,12]) rotate([-90,0,0]) cylinder(d=18,h=10);

}

module BMG_Aero_model()
{
  render()
  difference()
  {
    translate([-61.75,115.35,-156.9])
      import("BMG_Aero_Direct_Extruder.stl"); // https://www.thingiverse.com/thing:4226245
    *translate([18,-3,-18.7+3])
      rotate([0,90,0])
        cylinder(d=2,h=3,$fn=16);
    translate([18,-3-15,-18.7+3])
      rotate([0,90,0])
        cylinder(d=2,h=3,$fn=16);
    translate([18,-3-15-12,-18.7+3])
      rotate([0,90,0])
        cylinder(d=2,h=3,$fn=16);
  }
  translate(titan2bmgDiff+[0,-3,0])
    rotate([0,-90,90]) hotend(90);
  translate([-0.15,-44.5,-3.2])
    rotate([90,0,0])
      fan_parametric();
} 


module frame()
{
  offset = frameOffsetZ;
  
  color("silver", 0.2) render()
  {
    translate([-420/2,410/2,offset]) cube([420,30,30]);
    translate([-420/2,-410/2-30,offset]) cube([420,30,30]);

    translate([-420/2-30,-410/2,offset]) cube([30,410,30]);
    translate([420/2,-410/2,offset]) cube([30,410,30]);
    translate([-420/2-30,-410/2,offset-110-30]) cube([30,410,30]);
    translate([420/2,-410/2,offset-110-30]) cube([30,410,30]);
    translate([-420/2-30,-410/2,offset-500+30]) cube([30,410,30]);
    translate([420/2,-410/2,offset-500+30]) cube([30,410,30]);


    translate([420/2,410/2,offset-500+30]) cube([30,30,500]);
    translate([-420/2-30,410/2,offset-500+30]) cube([30,30,500]);
    
    translate([420/2,-410/2-30,offset-500+30]) cube([30,30,500]);
    translate([-420/2-30,-410/2-30,offset-500+30]) cube([30,30,500]);
    translate([-420/2,410/2,offset-500+30]) cube([420,30,30]);
    translate([-420/2,-410/2-30,offset-500+30]) cube([420,30,30]);
  }
/*
  if (cornerTest)
  {
    // prohibited area visualization. Risk of contact to Z axis holder
    color("red", 0.2) render()
    {
      translate([-420/2-30+62-30,-410/2,offset-113-30+32]) cube([30,410,30]);
      translate([420/2-62+30,-410/2,offset-113-30+32]) cube([30,410,30]);
    }
  }
*/
}

fanXoffset = 5-5;
fanYoffset = motorBodyLen-rodOffset+6+3+coolerY+1+1;
fanZoffset = 10+5;



module fanScrewHoleR(d=M3insertD, h=15)
{
  rotate([0,0,fanRotate])
  {
    translate([46.85,7.1,-15]) cylinder(d=d,h=h);
  }
}

module fanScrewHoleL(d=M3insertD, h=15)
{
  rotate([0,0,fanRotate])
  {
    translate([4.25,44.75,-15]) cylinder(d=d,h=h);
  }
}

module fanScrewHoles(d=M3insertD)
{
  fanScrewHoleL(d);
  fanScrewHoleR(d);
}


module frontHelper(len,topL,offsetX,offsetY,carryX,carryZ)
{
  hull()
  {
    translate([/*-offsetX*/-3,-offsetY-8-35/2,topL]) cube([60/2-offsetX+3,8,10]);
    translate([-3,-carryX/2,carryZ/2-0.1]) cube([len+15,carryX,0.1]);
  }
}
/*
module frontHelper2(len,topL,offsetX,offsetY,carryX,carryZ)
{
  hull()
  {
    #translate([-3,-offsetY-8-35/2,topL-5]) cube([60/2-offsetX+3,0.01,0.01]);
    translate([2,-carryX/2+8,carryZ/2-0.1]) cube([len+15-10,carryX/2-8,0.1]);
  }
}
*/

module frontMinus2(len,topL,offsetX,offsetY,carryX,carryZ)
{
  translate([-6,0,0]) rotate([0,90,0]) cylinder(d=motorFiletD,h=10);
  
  //motor
  hull()
  {
    intersection()
    {
      translate([motorWall-3,-44/2,-44/2]) cube ([len+30,44,44]);
      rotate([45,0,0]) translate([motorWall-3,-55/2,-55/2]) cube ([len+30,55,55]);
    }
    translate([0,0,-44/2+11]) cube ([len+30,0.01,44+2]);
  }
  // motor screws
  translate([-6,31/2,31/2]) rotate([0,90,0]) cylinder(d=M3hole,h=10);
  translate([-6,-31/2,31/2]) rotate([0,90,0]) cylinder(d=M3hole,h=10);
  translate([-6,31/2,-31/2]) rotate([0,90,0]) cylinder(d=M3hole,h=10);
  translate([-6,-31/2,-31/2]) rotate([0,90,0]) cylinder(d=M3hole,h=10);

  // top screws
  translate([25/2-offsetX,25/2-offsetY,topL-1]) cylinder(d=M3hole,h=15);
  //translate([-25/2-offsetX,25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);
  translate([25/2-offsetX,-25/2-offsetY,topL-1]) cylinder(d=M3hole,h=15);
  //translate([-25/2-offsetX,-25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);

  // M3 insert for interconnection PCB
  translate([25/2-offsetX+17,-offsetY,topL+3]) cylinder(d=M3insertD,h=10);
  //translate([25/2-offsetX-12,-offsetY,topL+3]) cylinder(d=M3insertD,h=10);
  
  translate([8,-27,-4]) wireHolder();
  translate([8+2,-27,15]) wireHolder();

  translate([8,-27,21]) rotate([-35,0,0])
  {
    translate([3,0.1,7]) wireHolder();
    translate([6,0.8,35]) wireHolder();
  }
  translate([8,-27,14+carryZ]) rotate([-90,0,0])
  {
    translate([7,0,35]) wireHolder();
    translate([7,0,60]) wireHolder();
  }

}  
module frontMinus1(len,topL,offsetX,offsetY,carryX,carryZ)
{
  //rail space
  difference()
  {
   hull()
    {
      translate([-offsetX-62/2,-offsetY-35/2,topL-13]) cube([62,36,13]);
      translate([-offsetX-62/2,-offsetY-1+35/2,20-7]) cube([62,1,0.01]);
    }
    translate([-offsetX+62/2,-offsetY-35/2,topL-13]) rotate([0,90+65,0]) cube(50);
  }
  intersection()
  {
    translate([-100/2,-offsetY-17/2,topL-16-50]) cube([100,17+50,11+50]);
    //hull()
    //{
    //  translate([-offsetX-100/2,-offsetY-35/2,topL-10]) cube([100,35,10]);
    //  translate([-offsetX-100/2,35/2-offsetY-0.01,23-15]) cube([100,0.01,0.01]);
    //}
    hull()
    {
      translate([-offsetX-110/2,-offsetY-35/2,topL-13]) cube([110,36,13]);
      translate([-offsetX-110/2,-offsetY-1+35/2,20-7]) cube([110,1,0.01]);
    }
  }
  //frontHelper2(len,topL,offsetX,offsetY,carryX,carryZ);
}



module front(len = 8 /*motorBodyLen+3*/,topL,offsetX,offsetY)
{
  blTxyz = bltouchPos - [headXoffset-rodOffset,-35,15];
  difference()
  {
    union()
    {
      difference()
      {
        union()
        {
          *translate([-3,-carryX/2,-carryZ/2]) 
            cube([len,carryX,carryZ]);
          hull()
          {
            translate([-3,-carryX/2,-carryZ/2]) cube([len,carryX,0.1]);
            translate([-3,-carryX/2,carryZ/2-0.01]) cube([len+20,carryX,0.01]);
          }
          *hull()
          {
            translate(blTxyz+[-9.5-8/2,-20-5,1]) cube([9+1+9.5+8,19,28]);
            translate([-offsetX+62/2-20,-offsetY-35/2-8,topL-13]) cube([4+20,(35+8)/2+3,10+11]);
          }
          translate([/*-offsetX*/-3,-offsetY-35/2,topL]) cube([62/2-offsetX+3,35-1,10-2]);
          *frontHelper(len,topL,offsetX,offsetY,carryX,carryZ);
          hull()
          {
            translate([-offsetX+62/2-20-4.5,-offsetY-35/2-8,topL-13]) cube([4+20+4.5,(35+8)/2+3,10+11]);
            translate([-3,-carryX/2,carryZ/2-0.1]) cube([len+20,carryX,0.1]);
          }
          translate([-offsetX+62/2-20,-offsetY-35/2-8,topL-13]) cube([4+20,(35+8-1),10+11]);
          //translate(blTxyz+[-9.5-8/2,-20-5,1]) cube([9+1+9.5+8,19,28]);
          
          // bltouch heat isolation
          translate([-3,carryX/2-8,-23.5]) cube([20.5,8,45]);
          
          // fan screw I
          *translate([-3,carryX/2-12,-carryZ/2-7]) cube([10,12,6]);
          // fan screw II
          *translate([-3,-(carryX/2),-carryZ/2-7]) cube([10,12,20]);

          // motor bottom frame
          translate([-3,-(carryX/2),-23.5]) cube([8,carryX,10]);
          
          if (rightAddon) hull()
          {
            translate([-3+8,(carryX/2)-10,-24]) cube([0.1,10,10]);
            //translate(carConn + [(len+20),-20-2,-42]) rotate([0,90,0]) cylinder (d=9,h=10);
            translate([-3+len+20-10,(carryX/2)-10+7,-24]) rotate([0,90,0]) cylinder (d=9,h=10);
          }
        }
        frontMinus1(len,topL,offsetX,offsetY,carryX,carryZ);
        // fan screw I
        *translate([-3+10/2,carryX/2-12+7,-carryZ/2-8]) cylinder(d=M3insertD,h=12);
        // fan screw II
        *translate([-3+10/2,-(carryX/2-12+7),-carryZ/2-8]) cylinder(d=M3insertD,h=12);
      }
      // connection to back
      intersection()
      {
        translate(carConn + [10,frontBackConn,0.5]) 
          rotate([-90,0,0]) 
            cylinder (d=5+5,h=7);
        translate([-3,-carryX/2,-carryZ/2]) 
          cube([len+10,carryX+10,carryZ+10]);
      }
    }
    frontMinus2(len,topL,offsetX,offsetY,carryX,carryZ);

    // connection to back()
    translate(carConn + [10,-20-2,0.5]) rotate([-90,0,0]) cylinder (d=M3insertD,h=29+6);
    //translate(carConn + [10,-50,0.5]) rotate([-90,0,0]) cylinder (d=M3hole,h=50);
    // connection to back()
    
    if (rightAddon)
    {
      translate([-3+len+20-7,(carryX/2)-10+7,-24]) rotate([0,90,0]) cylinder (d=M3insertD,h=29+6);
      translate([-3+len+20-7,(carryX/2)-10+7-5,topL+3]) rotate([0,90,0]) cylinder (d=M3insertD,h=29+6);
      translate([-3+len+20-7,(carryX/2)-10+7-5-22,topL-10]) rotate([0,90,0]) cylinder (d=M3insertD,h=29+6);
    }

    *translate(blTxyz)
    {
      translate([ 9,0,0]) cylinder (d=M3hole,h=10);
      translate([-9,0,0]) cylinder (d=M3hole,h=10);
      cylinder (d=5,h=10);
      translate([-9.5-8/2,-20-5+19-1.5,1-28]) cube([9+1+9.5+8,19,28]);
    }
  }
}
module cableHolder(len = 8 /*motorBodyLen+3*/,topL,offsetX,offsetY)
{
  difference()
  {
    union()
    {
      intersection()
      {
        union()
        {
          h = 23;
          translate([25/2-offsetX+12,25/2-offsetY+15,topL-6])
            cylinder (d=16,h=h);
          translate([25/2-offsetX+12,25/2-offsetY+15,topL-6])
            cylinder (d=17,h=1);
          translate([25/2-offsetX+12,25/2-offsetY+15,topL-6+h-1])
            cylinder (d=17,h=1);
        }
        translate([25/2-offsetX+13-16/2,33/2-offsetY,topL-6])
          cube([14,9,35]);
      }
      translate([25/2-offsetX+13-16/2,33/2-offsetY,topL+1])
          cube([14,8,8]);
      hull()
      {
        translate([25/2-offsetX-5-4+8,33/2-offsetY,topL+1])
          cube([28-8,6,8]);
        translate([25/2-offsetX-5,33/2-offsetY,topL+5])
          rotate([-90,0,0]) 
            cylinder (d=8,h=6);
      }
    }
    translate([25/2-offsetX+12,25/2-offsetY+16,topL-7])
      cylinder (d=20-10,h=35);
    translate([25/2-offsetX-5,33/2-offsetY,topL+5])
      rotate([-90,0,0]) 
        cylinder (d=M3hole,h=30);
    translate([25/2-offsetX-5,33/2-offsetY+4,topL+5])
      rotate([-90,0,0]) 
        cylinder (d=M3head,h=30);
    translate([25/2-offsetX+14,33/2-offsetY,topL+5])
      rotate([-90,0,0]) 
        cylinder (d=M3hole,h=30);
    translate([25/2-offsetX+14,33/2-offsetY+2,topL+5])
      rotate([-90,0,0]) 
        cylinder (d=M3head,h=30);
  }
}

ch_a = 
[
  //   x       y     z    angle
  [-12.5,      9,   10,   180],
  [-12.5,      9,   40,   180],
  [-12.5,      9,   50,   180],
  [  -10,  -5+40,  170, 90+30],
  [  -10, 229-40,  170, 90-45],
  [  -10,    229,   80,     0],
  [  -10,    229,   70,     0],
  [  -10,    229,   40,     0],
];

ch_c = gen_dat_ch(nSpline(ch_a,64));

function ch_f(d=pipeD-2, fn=64) = 
[
  for (i=[0:fn-1]) let (a=i*360/fn) (d/2) * [cos(a), sin(a)]
]; 
    
function gen_dat_ch(S) = 
   [ for (i=[0:len(S)-1]) 
       let(dat = Rx_(S[i][3], vec3D(ch_f()))) 
        T_(S[i][0], S[i][1], S[i][2], dat)]; 


module pipe()
{
  // just for visualization
  sweep(ch_c);
}

module pipeHeadHolder()
{
  xSize = pipeD+12;
  difference()
  {
    union()
    {
      intersection()
      {
        translate([-xSize/2-25/2,-35/2,0]) cube([xSize,35,5]);
        translate([-25/2,0,-1])
          rotate([0,0,45])
            {
              if (xSize > 27) 
                cylinder(d=xSize*1.2, h=10);
              else
                cylinder(d=27*1.2, h=10);
            //cube([xSize+5,xSize+5,10], center=true);
            }
      }
      *difference()
      {
        translate([-25/2-25/2,-35/2,0]) cube([25,5,65]);
        translate([-25/2,-25/2,-1]) cylinder(d=M3head,h=25);
      }
      *hull()
      {
        translate([-25/2-25/2,-35/2,0]) cube([5,35,5]);
        translate([-25/2-25/2,-35/2,0]) cube([5,5,65]);
      }
      *translate([-25/2-25/2-10,-35/2+5,0]) cube([10.1,5,19]);
      translate([-25/2,-25/2+3,0])
      difference()
      {
        hull()
        {
          intersection()
          {
            cylinder(d=xSize,h=35);
            translate([-(xSize+2)/2,0,-1])
              cube([xSize+2,pipeD+10+2,52]);
          }
          translate([-xSize/2,0,0]) 
            cube([xSize,35-8-10,5]);
        }
        translate([0,0,-1])
          cylinder(d=pipeD,h=52);
        translate([0,0,17])
          pipeBandHole();
        translate([0,0,27])
          pipeBandHole();

      }
    }
    translate([-25/2,-25/2,-1]) cylinder(d=M3hole,h=10);
    translate([-25/2,25/2,-1]) cylinder(d=M3hole,h=10);
    hull()
    {
      translate([-25/2,25/2,5]) 
        cylinder(d=M3head,h=50);
      translate([-25/2,25/2+20,5]) 
        cylinder(d=M3head,h=50);
    }
    translate([-25/2-xSize/2-1,-2,10]) rotate([90,0,0]) wireHolder();
    translate([-25/2+xSize/2+1,-2,10]) rotate([90,0,0]) wireHolder();
  }
}

ch2_a = 
[
  //   x     y     z    sy    sz   angle
  [   -15,  -13,    40,   20,  15,  0],
  [  150,    10,    40,   20,  15,  0],
  [  200,   100,   40,   20,  15,  90],
  [  150,   220,   40,   20,  15,  180],
  [    0,   240,   40,   20,  15,  180],
];

ch2_c = gen_dat_ch2(nSpline(ch2_a,64));

function ch2_f(sy, sz) = 
[
  [0,0],
  [0,sz],
  [sy,sz],
  [sy,0]
]; 
    
function gen_dat_ch2(S) = 
   [ for (i=[0:len(S)-1]) 
       let(dat = Rz_(S[i][5], Ry_(90,vec3D(ch2_f(S[i][3],S[i][4]))))) 
        T_(S[i][0], S[i][1], S[i][2], dat)]; 


module chain()
{
  sweep(ch2_c);
}

module chainHeadHolder()
{
  difference()
  {
    union()
    {
      translate([-25/2-25/2,-35/2,0]) cube([25,35,5]);
      difference()
      {
        translate([-25/2-25/2,-35/2,0]) cube([25,5,65]);
        translate([-25/2,-25/2,-1]) cylinder(d=M3head,h=25);
      }
      hull()
      {
        translate([-25/2-25/2,-35/2,0]) cube([5,35,5]);
        translate([-25/2-25/2,-35/2,0]) cube([5,5,65]);
      }
      translate([-25/2-25/2-10,-35/2,0]) cube([10.1,5,25]);
    }
    translate([-25/2,-25/2,-1]) cylinder(d=M3hole,h=10);
    translate([-25/2,25/2,-1]) cylinder(d=M3hole,h=10);
  }
}


module chainHeadHolder2()
{
  xSize = 24;
  difference()
  {
    union()
    {
      intersection()
      {
        translate([-xSize/2-25/2,-35/2,0]) cube([xSize,35,5]);
        translate([-25/2,0,-1])
          rotate([0,0,45])
            {
              if (xSize > 27) 
                cylinder(d=xSize*1.2, h=10);
              else
                cylinder(d=27*1.2, h=10);
            //cube([xSize+5,xSize+5,10], center=true);
            }
      }
      translate([-25/2,-25/2+(pipeD)/2-5,0])
      difference()
      {
        hull()
        {
          intersection()
          {
            cylinder(d=xSize,h=35);
            translate([-(xSize+2)/2,0,-1])
              cube([xSize+2,pipeD+10+2,52]);
          }
          translate([-xSize/2,0,0]) 
            cube([xSize,35-(pipeD)/2-10,5]);
        }

      }
    }
    translate([-25/2,-25/2,-1]) cylinder(d=M3hole,h=10);
    translate([-25/2,25/2,-1]) cylinder(d=M3hole,h=10);
    hull()
    {
      translate([-25/2,25/2,5]) 
        cylinder(d=M3head,h=50);
      translate([-25/2,25/2+20,5]) 
        cylinder(d=M3head,h=50);
    }
    translate([-xSize+1,-2,10]) rotate([90,0,0]) wireHolder();
    translate([ 3,-2,10]) rotate([90,0,0]) wireHolder();
  }
}

module bandTensionInnerSpace(carOffset)
{
  translate([-30+carOffset,-5-1+10,idler1posZ]) bandTensionerHole();
  mirror([1,0,0]) translate([-30-carOffset,-5-1+10,idler1posZ]) bandTensionerHole(); 
  translate([-30+carOffset,-5-1+10,idler2posZ]) bandTensionerHole();
  mirror([1,0,0]) translate([-30-carOffset,-5-1+10,idler2posZ]) bandTensionerHole();
}

module backFanDish(len,topL,offsetX,offsetY)
{
  difference()
  {
      translate([fanXoffset, fanYoffset+(5+2.5-1),fanZoffset] + [0,-14-1+5+(2.5-0.5),0] + carTranformation)
        rotate([90,0,180])
          fanScrewHoleL(d=9, h=1.5);
      translate([fanXoffset, fanYoffset,fanZoffset] + [0,-14,0] + carTranformation)
        rotate([90,0,180])
          fanScrewHoles(M3hole);
  }
}

module back(len,topL,offsetX,offsetY)
{
  montageSpace = 6;
  
  difference()
  {
    union()
    {
      //translate([-3-60/2,-offsetY-35/2,topL]) cube([60/2-offsetX,35,8]);
      
      translate([-60/2-10+0.2,-offsetY-35/2,topL]) cube([60/2-3-0.2+10-0.2,35+1,8]);
      //translate([-60/2+carOffset,-1+9,22]) cube([60,8+3,20]);
      translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-3]) 
        cube([60-21-2*montageSpace,5+2.5-0.5,21+3+6]);
      *hull()
      {
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-3]) 
          cube([10,5+2.5-0.5,0.1]);
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace+(10-4)/2,-1+9+5,21-3-15]) 
          cube([4,5+2.5-0.5-5,0.1]);
      }
      *hull()
      {
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-3]) 
          cube([10,5+2.5-0.5,0.1]);
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace+(10-4)/2,-1+9+5,21-3-15]) 
          cube([4,5+2.5-0.5-5,0.1]);
      }
      intersection() 
      {
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9-17,21-7.5-10-10]) 
          cube([60-21-2*montageSpace+5,5+2.5-0.5+17,7+10-idler2idlerPitch+10]);
        union()
        {
          hull() // bltouch
          {
            translate(bltouchPos_mot+[0,9,0]) cylinder(d=10,h=10);
            translate(bltouchPos_mot+[0,0,0]) cylinder(d=12,h=2);
            translate(bltouchPos_mot+[0,-9,0]) cylinder(d=10,h=7);
            *translate(carTranformation + [-60/2+carOffset+21/2+montageSpace+13-5,-1+9,21-4.5]) 
              cube([60-21-2*montageSpace-13+5,5+2.5-0.5,5]);
            translate(carTranformation + [-1,-1+9-16,15]) 
              cube([2.5,5+2.5-0.5+16,4]);
          }
          hull() // fan tunnel
          {
            translate(carTranformation + [-1,-1+9-16,15]) 
              cube([2.5,5+2.5-0.5+16,4]);
            translate(carTranformation + [-5,-1+9+5-7,21-3-15]) 
              cube([7,7+2.5-0.5-5+5,20]);
          }
        }
        union()
        {
          translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9-20,21-6.5-20]) 
            cube([60-21-2*montageSpace+5,5+2.5-0.5+20,5+20]);
          translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-6.5-20]) 
            cube([60-21-2*montageSpace+5,5+2.5-.5,6.5+2+20]);
        }
      }
      *intersection()
      {
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9-17,21-7.5-10]) 
          cube([60-21-2*montageSpace+5,5+2.5-0.5+17,7+10-idler2idlerPitch]);

        hull() // fanduct() connection
        {
          translate(fanscrew_1_mot) rotate([0,90,0]) cylinder(d=10,h=7);
          translate(fanscrew_2_mot) rotate([0,90,0]) cylinder(d=10,h=7);
          translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-1]) 
             cube([7+6,5+2.5-0.5,1]);
          translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,21-8]) 
             cube([7,5+2.5-0.5,7]);
        }
      }

      translate(carTranformation + [-(20-0.4)/2,-1+9-3,bandMiddleZ-(2*idlerH+idler2idlerPitch-3.4)/2]) 
        cube([20-0.4,5+2.5-0.5,2*idlerH+idler2idlerPitch-3.4]);

      *translate([-60/2+(60/2-3)/2-23.5,-1+9-(-35),topL]) 
        cube([(60/2-3)/2-0.2+35,5+2.5-0.5,8]);
      *#translate([-10,-1+9-(-35),topL]) 
        cube([35,5+2.5-0.5,8]);
      if (cableHolder2) 
      {
        hull()
        {
          // cable holder
          translate([25/2-offsetX-5,-1+9-(-35),topL+5])
            rotate([-90,0,0]) 
              cylinder (d=15,h=5+2.5-0.5);
          translate([25/2-offsetX+14,-1+9-(-35),topL+5])
            rotate([-90,0,0]) 
              cylinder (d=15,h=5+2.5-0.5);
          translate([25/2-offsetX-5,-1+9-(-35),topL+5-15])
            rotate([-90,0,0]) 
              cylinder (d=15,h=5+2.5-0.5);
        }
      }
      else
      {
        *translate([25/2-offsetX,-1+9-(-35)+2,topL-18])
           rotate([-90,45,0]) 
            cylinder (d=11,h=5+2.5-0.5-2,$fn=4);
      }
      hull()
      {
        translate(carTranformation + [-60/2+carOffset+21/2+montageSpace,-1+9,22+15+6]) 
          cube([60-21-2*montageSpace,5+2.5-0.5,20]);
        translate([-60/2+(60/2-3)/2-23-0.5+0.2,-1+9-(-35),topL]) 
          cube([(60/2-3)/2-0.2+23+0.5-0.2,5+2.5-0.5,8]);
      }
      // fan left screw
      hull()
      {
        translate([fanXoffset, fanYoffset+(5+2.5-1+2),fanZoffset] + [0,-14-1,0] + carTranformation)
          rotate([90,0,180])
            fanScrewHoleL(d=8, h=5);
        translate([fanXoffset-20, fanYoffset+(5+2.5-1+2),fanZoffset] + [0,-14-1,0] + carTranformation)
          rotate([90,0,180])
            fanScrewHoleL(d=8, h=5);
      }
      translate([fanXoffset, fanYoffset+(5+2.5-1),fanZoffset] + [0,-14-1,0] + carTranformation)
        rotate([90,0,180])
          fanScrewHoleL(d=10, h=5+2.5-0.5);
      // endstop sensor
      translate([-60/2-10-10,-offsetY+6,topL]) 
        cube([11,16+2.5-0.5,3]);
      hull()
      {
        translate([-60/2-10-10,-offsetY+6,topL]) 
          cube([10,16+2.5-0.5,3]);
        translate([-60/2-10-10,-offsetY+6,topL-3]) 
          cube([10,16+2.5-0.5-10,3]);
      }
      translate([-60/2+(60/2-3)/2-23-0.5-10,-1+9-(-35)+5,topL]) 
        cube([(60/2-3)/2-0.2+23+0.5,2/*5+2.5-0.5*/,8]);
    }
    
    // top screws
    //translate([25/2-offsetX,25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);
    translate([-25/2-offsetX,25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);
    //translate([25/2-offsetX,-25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);
    translate([-25/2-offsetX,-25/2-offsetY,topL-1]) cylinder(d=M3hole,h=10);
    
    // fan deflector screws
    translate([-15-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2insertD,h=20);
    *translate([-5-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2insertD,h=20);
    *translate([5-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2insertD,h=20);
    *translate([15-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2insertD,h=20);
    translate([10-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2insertD,h=20);

    translate(carTranformation) bandTensionInnerSpace(carOffset);
     
    translate([fanXoffset, fanYoffset-3.5,fanZoffset] + [0,-14,0] + carTranformation)
      rotate([90,0,180])
        fanScrewHoles(M3insertD);
    translate([fanXoffset, fanYoffset,fanZoffset] + [0,-14,0] + carTranformation)
      rotate([90,0,180])
        fanScrewHoles(M3hole);
    
    // connection to tensioner R
    translate(carConn + [10,7-2.5-10.2,0]) rotate([-90,0,0]) cylinder (d=M3hole,h=10);
    translate(carConn + [10,7-2.5,0]) rotate([-90,0,0]) cylinder (d=M3head,h=10);
    // connection to tensioner L - for M3 insert
    translate(carConn + [-10,7-5.5-10.2,0]) rotate([-90,0,0]) cylinder (d=M3hole,h=10);
    translate(carConn + [-10,7-5.5,0]) rotate([-90,0,0]) cylinder (d=M3insertD,h=10);

    // fan M3 inserts
    translate(fanscrew_1_mot+[-1,0,0]) rotate([0,90,0]) cylinder(d=M3insertD,h=8);
    translate(fanscrew_2_mot+[-1,0,0]) rotate([0,90,0]) cylinder(d=M3insertD,h=8);

    // M3 insert for interconnection PCB
    *translate([-25/2-offsetX-12,-offsetY,topL+1]) cylinder(d=M3insertD,h=10);
    *translate([-25/2-offsetX+3,-offsetY,topL+1]) cylinder(d=M3insertD,h=10);

    // endstop sensor
    translate([-25/2-offsetX-12-11,-offsetY+9.5,topL-10]) cylinder(d=M3insertD_Xsensor,h=20);

    // bltouch
    translate(bltouchPos_mot+[0,9,-1]) cylinder(d=M3insertD_bltouch,h=7.5);
    translate(bltouchPos_mot+[0,0,-1]) cylinder(d=6,h=3+1);
    translate(bltouchPos_mot+[0,-9,-1]) cylinder(d=M3insertD_bltouch,h=7.5);
    
    if (cableHolder2)
    {
      // cable holder
      translate([25/2-offsetX-5,33/2-offsetY-7,topL+5])
        rotate([-90,0,0]) 
          cylinder (d=M3insertD,h=30);
      translate([25/2-offsetX+14,33/2-offsetY-7,topL+5])
        rotate([-90,0,0]) 
          cylinder (d=M3insertD,h=30);
    }
    else
    {
      *translate([5,51,27.15]) wireHolder();
      *translate([-3,51,46]) rotate([0,-45]) wireHolder();
      *translate([-33,51,49]) wireHolder();
    }
    translate(-motor2absTransform+[10,11,9])
      rotate([0,-90,0])
      {
        cylinder (d=M3insertD,h=30);
      }
  }
}

/*
module fanSpacer(len,topL,offsetX,offsetY)
{
  translate([0.75,15-2,54.75]) rotate([-90,0,0]) 
  difference()
  {
    cylinder(d=8,h=2.5);
    cylinder(d=M3hole,h=5);
    //translate([fanXoffset, fanYoffset,fanZoffset] + [0,-14,0] + carTranformation +[headXoffset-rodOffset,-35,15])
      rotate([90,0,180])
        fanScrewHoles(d=M3hole);
  }
}*/

module airDeflector(topL,offsetX,offsetY)
{
  m = bandMiddleZ;
  difference()
  {
    union()
    {
      hull()
      {
        translate([motor2absTransform[0]+(-60/2-10-9),-3-2,m-8/2-2]) 
          cube([60/2-3-0.2+10+9,2,8+1]);
        translate([motor2absTransform[0]+(-60/2-10-9),-3-1,m-8/2+16+1-10]) 
          rotate([0,90,0])
            cylinder(d=1.2, h=60/2-3-0.2+10+9); 
      }
      *translate([motor2absTransform[0]+(-60/2-10-9),-3-2,m-8/2-2]) 
        cube([60/2-3-0.2+10+9,2,8+1]);
      *translate([motor2absTransform[0]+(-60/2-10-9),-3-14,m-8/2+16]) 
        cube([60/2-3-0.2+10+9,14,2]);
      hull()
      {
        translate([motor2absTransform[0]+(-60/2-10-9),-3-1,m-8/2+16+1-10]) 
          rotate([0,90,0])
            cylinder(d=1.2, h=60/2-3-0.2+10+9); 
        translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01+1-0.4,topL-13]) 
          rotate([0,90,0])
            cylinder(d=1.2, h=60/2-3-0.2+10+9); 
      }

      hull()
      {
        *translate([motor2absTransform[0]+(-60/2-10-9),-3-8,m-8/2-2]) 
          cube([1.2,8,8+10+2]);
        translate([motor2absTransform[0]+(-60/2-10-9),-3-8+4,m-8/2-2]) 
          rotate([0,90,0])
            cylinder(d=8, h=1.2); 
        translate([motor2absTransform[0]+(-60/2-10-9),-3-8+4,m-8/2-2+12+4]) 
          rotate([0,90,0])
            cylinder(d=8, h=1.2); 
      }
      hull()
      {
        translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01,topL+2]) 
          cube([60/2-3-0.2+10+9,2,8-1-2]);
        translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01+1-0.4,topL-13]) 
          rotate([0,90,0])
            cylinder(d=1.2, h=60/2-3-0.2+10+9); 
        *translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01,topL-15+2]) 
          cube([60/2-3-0.2+10+9,1.2,1]);
      }
      translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01,topL-15-3]) 
        cube([1.2,8,8+15-5+3]);

      hull()
      {
        translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01+4,topL-15-3]) 
          rotate([0,90,0])
            cylinder(d=8, h=1.2); 
        translate([motor2absTransform[0]+(-60/2-10-9),-3-4,m-8/2+16-12]) 
          rotate([0,90,0])
            cylinder(d=8, h=1.2); 
      }
      translate([motor2absTransform[0]+(-60/2-10-9),-3-16,m-8/2+16-6]) 
        cube([1.2,16-4,8]);
      translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01,topL-15+18-6]) 
        cube([8,13.5,6]);
      translate(motor2absTransform+[-60/2-10-9,-offsetY-35/2-2.01,topL-15+18-5]) 
        cube([8,7,5+4-1.5]);
    }
  
    // fan reflector screws
    translate(motor2absTransform) 
    {
      translate([-15-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2hole,h=20);
      *translate([-5-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2hole,h=20);
      *translate([5-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2hole,h=20);
      *translate([15-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2hole,h=20);
      translate([10-25/2-offsetX,-25/2-offsetY,topL+8/2]) rotate([90,0,0]) cylinder(d=M2hole,h=20);
    }
    //tensioner screws
    translate([carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2hole,h=10);
    *translate([22+carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2hole,h=10);
    translate([-22+carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2hole,h=10);
    translate([-25/2-offsetX,-25/2-offsetY,topL-1]) cylinder(d=M3head,h=10);
    translate([-10+carOffset,1,m]) rotate([90,0,0]) cylinder (d=M3head,h=10);
    
    // x sensor
    translate(motor2absTransform + [-15-25/2-offsetX-8,-25/2-offsetY+3,topL-10]) 
      cylinder(d=M3insertD_small,h=20);
  }
}

module tensioner(len,topL,offsetX,offsetY)
{
  m = bandMiddleZ;
  h2 = idlerH+idler2idlerPitch/2+2;
  difference()
  {
    union()
    {
      translate([-60/2+carOffset,-8-1+9-3,m-(2*idlerH+idler2idlerPitch)/2])
        cube([60,8+3,2*idlerH+idler2idlerPitch]);
      hull()
      {
        translate([30-3.5+carOffset,-8-1+12,m+h2])
          rotate([-90,0,0]) cylinder(d=9,h=5);
        translate([30-3.5+carOffset,-8-1+12,m-h2])
          rotate([-90,0,0]) cylinder(d=9,h=5);
        translate([60/2-7/2+2+carOffset,-8-1+6,m+h2-3])
          rotate([-90,0,0]) cylinder(d=3,h=0.01);
        translate([60/2-7/2+2+carOffset,-8-1+6,m-h2+3])
          rotate([-90,0,0]) cylinder(d=3,h=0.01);
      }
      hull()
      {
        translate([-30+3.5+carOffset,-8-1+12,m+h2])
          rotate([-90,0,0]) cylinder(d=9,h=5);
        translate([-30+3.5+carOffset,-8-1+12,m-h2])
          rotate([-90,0,0]) cylinder(d=9,h=5);
        translate([-60/2+7/2-2+carOffset,-8-1+6,m+h2-3])
          rotate([-90,0,0]) cylinder(d=3,h=0.01);
        translate([-60/2+7/2-2+carOffset,-8-1+6,m-h2+3])
          rotate([-90,0,0]) cylinder(d=3,h=0.01);
      }
    }
    bandTensionInnerSpace(carOffset);
    translate([30-3.5+carOffset,2,m+h2]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
    translate([30-3.5+carOffset,2,m-h2]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
    translate([-30+3.5+carOffset,2,m+h2]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
    translate([-30+3.5+carOffset,2,m-h2]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
    if (idler2idlerPitch >= 2)
    {
      translate([30-3.5+carOffset,2,m]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
      translate([-30+3.5+carOffset,2,m]) rotate([-90,0,0]) cylinder(d=M3insertD,h=50);
    }
    translate([carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2insertD,h=10);
    translate([22+carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2insertD,h=10);
    translate([-22+carOffset,1,m]) rotate([90,0,0]) cylinder(d=M2insertD,h=10);

//    #translate([fanXoffset, fanYoffset,fanZoffset])
//      rotate([90,0,180])
//        fanScrewHoles();
    //translate(carConn)
    translate([10,-5,m])  
      rotate([-90,0,0]) 
        cylinder (d=M3hole,h=30);
    translate([-10,-5,m])  
      rotate([-90,0,0]) 
        cylinder (d=M3hole,h=30);
    
    //translate([-10,offsetY-frontBackConn-1+1,m]) 
    //  rotate([-90,0,0]) 
    //    cylinder (d=M3insertD,h=5);

    translate([-20/2+carOffset,-8-1+9-3+8+3-3,m-(2*idlerH+idler2idlerPitch-3)/2]) 
      cube([20,5+2.5-0.5,2*idlerH+idler2idlerPitch-3]);

    }
}

module tensionerClampL(len,topL,offsetX,offsetY)
{
  h2 = idlerH+idler2idlerPitch/2+2;
  difference()
  {
    union()
    {
      // translate([-60/2,-8-1+9-3+8+3,22-5]) cube([9,4,30]);
      hull()
      {
        translate([-30+3.5,3+5,m+h2]) rotate([-90,0,0]) cylinder(d=9,h=6);
        translate([-30+3.5,3+5,m-h2]) rotate([-90,0,0]) cylinder(d=9,h=6);
      }
      *hull()
      {
        translate([-30+3.5-15.35,3+5,m-h2-2.35+0.5+idler2idlerPitch])
          rotate([-90,0,0]) cylinder(d=9,h=6);
        translate([-30+3.5,3+5,m-h2]) rotate([-90,0,0]) cylinder(d=9,h=6);
        translate([-30+3.5,3+5,m-h2+4]) rotate([-90,0,0]) cylinder(d=9,h=6);
      }
      *translate([-30+3.5-15.35,3+5,m-h2-2.35+0.5+idler2idlerPitch]) 
        rotate([-90,0,0]) cylinder(d=9,h=6+1.5);
      // fan screw
      translate([fanXoffset, fanYoffset+(5+2.5-1-1),fanZoffset] + [0,-14,0])
        rotate([90,0,180])
          fanScrewHoleR(d=9, h=5+2.5+1);
      hull()
      {
        translate([fanXoffset, fanYoffset+(5+2.5-1-1),fanZoffset] + [0,-14,0])
          rotate([90,0,180])
            fanScrewHoleR(d=9, h=6);
        translate([-30+3.5,3+5,m+h2]) rotate([-90,0,0]) cylinder(d=9,h=6);
      }

    }
    m = bandMiddleZ;
    translate([-30+3.5,2,m+h2]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
    translate([-30+3.5,2,m-h2]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
    translate([-30+3.5,3+5+3,m+h2]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    translate([-30+3.5,3+5+3,m-h2]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    if (idler2idlerPitch >= 2)
    {
      translate([-30+3.5,2,m]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
      translate([-30+3.5,3+5+3,m]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    }

    *translate([fanXoffset, fanYoffset,fanZoffset] + [0,-14,0] + carTranformation +[headXoffset-rodOffset,-35,15])
      rotate([90,0,180])
        fanScrewHoles();
    *translate ([fanXoffset-28+7/2-fanWall,3+4.5/*fanYoffset-20+2*/,fanZoffset-7/2]) 
      rotate([-90,0,0]) 
        cylinder(d=M3hole,h=(fanYoffset-20+2)-(3+4.5));
    *translate([fanXoffset, fanYoffset+(5+2.5-1)+5.5+0.2,fanZoffset] + [0,-14,0])
      rotate([90,0,180])
        fanScrewHoleR(d=M3hole, h=10);
    *translate([fanXoffset, fanYoffset+(5+2.5-1-1),fanZoffset] + [0,-14,0])
      rotate([90,0,180])
        fanScrewHoleR(h=5.5);
    translate([fanXoffset, fanYoffset+(5+2.5-1-1),fanZoffset] + [0,-14,0])
      rotate([90,0,180])
        fanScrewHoleR(h=10);

  }
}

module tensionerClampR(len,topL,offsetX,offsetY)
{
  h2 = idlerH+idler2idlerPitch/2+2;
  difference()
  {
    hull()
    {
      translate([30-3.5,3+5,m+h2]) rotate([-90,0,0]) cylinder(d=9,h=4+2);
      translate([30-3.5,3+5,m-h2]) rotate([-90,0,0]) cylinder(d=9,h=4+2);
    }
    m = bandMiddleZ;
    translate([30-3.5,2,m+h2]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
    translate([30-3.5,2,m-h2]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
    translate([30-3.5,3+5+3,m+h2]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    translate([30-3.5,3+5+3,m-h2]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    if (idler2idlerPitch >= 2)
    {
      translate([30-3.5,2,m]) rotate([-90,0,0]) cylinder(d=M3hole,h=50);
      translate([30-3.5,3+5+3,m]) rotate([-90,0,0]) cylinder(d=M3head,h=50);
    }
  }
}



function fd_f(sy, sz, d) = 
[
  [0-d,0-d],
  [0-d,sz+d],
  [sy+d,sz+d],
  [sy+d,0-d]
]; 
    
function gen_dat_O(S,d) = 
   [ for (i=[1:len(S)-1]) 
       let(dat = Rx_(90+S[i][5], vec3D(fd_f(S[i][3],S[i][4],d)))) 
        T_(S[i][0]-23, S[i][1], S[i][2], dat)]; 
function gen_dat_I(S,d) = 
   [ for (i=[0:len(S)-1]) 
       let(dat = Rx_(90+S[i][5], vec3D(fd_f(S[i][3],S[i][4],d)))) 
        T_(S[i][0]-23, S[i][1], S[i][2], dat)]; 

function fd_s(sy, sz, d) = 
[
  [0-d,sz/2-d/2],
  [0-d,sz/2+d/2],
  [sy+d,sz/2+d/2],
  [sy+d,sz/2-d/2]
]; 
    
function gen_dat_s(S) = 
   [ for (i=[12:len(S)-20]) 
       let(dat = Rx_(90+S[i][5], vec3D(fd_s(S[i][3],S[i][4],S[i][6])))) 
        T_(S[i][0]-23, S[i][1], S[i][2], dat)]; 



module idlerHole(d=idlerHoleDia, h=idlerH+1+0.2, screw=1)
{
  difference()
  {
    hull()
    {
      translate([0,0,-h/2]) cylinder(d=d,h=h);
      translate([100,-d/2,-h/2]) cube([0.01,d,h]);
    }
    translate([0,0,-h/2]) cylinder(d1=7,d2=5,h=0.5);
    translate([0,0,h/2-0.5]) cylinder(d2=7,d1=5,h=0.5);
  }
  if (screw)
  {
    translate([0,0,h/2+1]) cylinder(d=M3thread,h=5);
    translate([0,0,-h/2-3]) cylinder(d=M3hole,h=h+3+1);
    translate([0,0,-h/2-3-50]) cylinder(d=M3head,h=50);
  }
}

module bandHole1()
{
  translate([0,-5.5/2-idlerPitch/2,-(bandW+2)/2]) cube([100,5,bandW+2]);
  translate([-5.5/2-idlerPitch/2,0,-(bandW+2)/2]) cube([5,100,bandW+2]);
}
module bandHole2()
{
  translate([-8/2-idlerPitch/2,-100,-(bandW+2)/2]) cube([8,200,bandW+2]);
}
module xyScrews(orientation)
{
  aaa = 5*orientation;
    translate([10,10,-50+aaa])
  { // rear right
    cylinder(d=M3hole,h=60);
    if (orientation!=1) 
      cylinder(d=M3head,h=34);
    else
      translate([0,0,19+0.5+3-30])
        cylinder(d=M3head,h=10.5+30);
  }
  translate([-10,10,-50-aaa]) 
  { // rear left
    cylinder(d=M3hole,h=60);
    cylinder(d=M3head,h=34);
  }
  translate([10,-10,-50+aaa]) 
  { // front right
    cylinder(d=M3hole,h=60);
    if (orientation!=1) 
      cylinder(d=M3head,h=34);
    else
      translate([0,0,19+0.5+3])
        cylinder(d=M3head,h=10.5);
  }
  translate([-10,-10,-50-5]) 
  { // front left
    cylinder(d=M3hole,h=60);
    if (orientation==1) 
      cylinder(d=M3head,h=34);
    else
      translate([0,0,19+0.5+3-30+6.5])
        cylinder(d=M3head,h=10.5+30);
  }
  translate([-orientation*2.5,50,-(10-3.5)]) rotate([90,0,0]) 
  { 
    cylinder(d=M3hole,h=50);
    cylinder(d=M3head,h=33);
    translate([0,0,50-(4+15/2)-1])cylinder(d=M3insertD,6+15/2);//(d=5.5/cos(30),h=4+15/2,$fn=6);
  }
  translate([orientation*20,50,-(10-3.5)]) rotate([90,0,0]) 
  { 
    cylinder(d=M3hole,h=50);
    cylinder(d=M3head,h=33);
    translate([0,0,50-(4+15/2)-1])cylinder(d=M3insertD,6+15/2); //(d=5.5/cos(30),h=4+15/2,$fn=6);
  }
}

module XrailHole()
{
  width = 15.08;
  heigth = 10.01;
  translate([-50,-width/2,-10]) cube([200,width,heigth]);
}

module xyL(zPos)
{
  difference()
  {
    union()
    {
      translate([-27/2-1, -35/2, -15.5]) cube([27+1+20,35+5,15.5]);
      //translate([-27/2, -23/2-5, -5-8]) cube([20+27,23+5,5+8]);
      hull()
      {
        translate([-27/2-1, -(mgnPlusY-(idlerPitch)/2)-22+23/2, -(29+9+1+idler2idlerPitch-1)])
          cube([8,23,29+9+1+idler2idlerPitch-1]);
        translate([-27/2-1, -35/2, -15.5-3])
          cube([8,35+5,15.5+3]);

      }
      translate([-27/2-1, -(mgnPlusY-(idlerPitch)/2)-22+23/2, -(29+9+1-1)]) cube([15+1,23,29+9+1-1]);
      hull()
      {
        *hull()
        {
          translate([-27/2-1, -(mgnPlusY-(idlerPitch)/2)-22+23/2, -(29+9+1-1)]) cube([8,23,29+9+1-1]);
          translate([-27/2-1, -35/2, -15.5-3]) cube([8,35+5,15.5+3]);

        }

        translate([-27/2+5, -(mgnPlusY-(idlerPitch)/2)-22+23/2, -(29+9+1+idler2idlerPitch-1)]) 
          cube([5,23,29+9+1+idler2idlerPitch-1]);
        //translate([27/2+15, -23+23/2-(mgnPlusY+(idlerPitch)/2), -29]) cube([5,23,29-0.2]);
        translate([27/2+15, -7+23/2-(mgnPlusY+(idlerPitch)/2), -(29+9+1+idler2idlerPitch-3)])
          cube([5,7,29+9+1+idler2idlerPitch-3]);

        translate([-bandAxisDevianceX+(idlerPitch), -(mgnPlusY+(idlerPitch)/2), -29/*-9-1*/])
          cylinder(d=25, h=29/*+9+1*/);
        translate([-bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), -(29+9+1+idler2idlerPitch)])
          cylinder(d=25, h=29+9+1+idler2idlerPitch);
      }
    }
    XrailHole();
    translate([-bandAxisDevianceX+idlerPitch, -(mgnPlusY+(idlerPitch)/2), idler2posZ - zPos])
    { 
      rotate([0,0,0]) idlerHole();
      rotate([0,0,-90]) bandHole1();
    }
    translate([-bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), idler1posZ - zPos])  
    {
      rotate([0,0,90]) idlerHole();
      bandHole1();
    }
    translate([-bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), idler2posZ - zPos])
    {
      bandHole2();
    }
    xyScrews(1);
    
    // endstop flag screws
    translate([27, 15/2+4,-8]) 
      cylinder(d=M3insertD,h=50);
    if (!lightFlags)
      translate([27, -(15/2+4),-8]) 
        cylinder(d=M3insertD,h=50);

     translate([-(15-2), -35/2+0.5,-7]) rotate([90,0,0])
     {
       linear_extrude(height = 1)
        text("thingiverse.com", size=4.7);
     } 
     translate([-(15-3), -35/2+0.5,-13]) rotate([90,0,0])
     {
       linear_extrude(height = 1)
        text("/thing:4663434", size=4.7);
     } 
/*     translate([-(15-1),  29,-15]) rotate([90,0,-90])
     {
       linear_extrude(height = 1)
        text("thingiverse.com", size=4.7);
     } 
     translate([-(15-1),  29,-21]) rotate([90,0,-90])
     {
       linear_extrude(height = 1)
         text("/thing:3786629", size=4.7);
     }
     translate([-(15-1.5),  25,-9]) rotate([90,0,-90])
    */ 
     /*translate([-(15-1),  25,-22]) rotate([90,0,-90])
     {
       linear_extrude(height = 2)
         text("L", size=18);
     }*/
     translate([-5, 10, -0.5])
     {
       linear_extrude(height = 1)
         text("L", size=10);
     } 

  }
}
module xFlag(XflagLen=XflagLen)
{
  tunning = 4;
  difference()
  {
    union()
    {
      hull()
      {
        translate([27-tunning, 15/2+4,0]) cylinder(d=8,h=4);
        translate([27+tunning, 15/2+4,0]) cylinder(d=8,h=4);
        translate([27+tunning, 0,0]) cylinder(d=8,h=4);
      }
      if (!lightFlags) hull()
      {
        translate([27-tunning, -(15/2+4),0]) cylinder(d=8,h=4);
        translate([27+tunning, -(15/2+4),0]) cylinder(d=8,h=4);
        translate([27+tunning, 0,0]) cylinder(d=8,h=4);
      }
      hull()
      {
        translate([28,-5/2,0]) cube([0.01,5,4]);
        translate([34,-2/2,0]) cube([0.01,2,7]);
      }
      translate([34,-2/2,0]) cube([XflagLen,2,7]);
    }
      hull()
      {
        translate([27-tunning, 15/2+4,-1]) cylinder(d=M3hole,h=7);
        translate([27+tunning, 15/2+4,-1]) cylinder(d=M3hole,h=7);
      }
      hull()
      {
        translate([27-tunning, -(15/2+4),-1]) cylinder(d=M3hole,h=7);
        translate([27+tunning, -(15/2+4),-1]) cylinder(d=M3hole,h=7);
      }
  }
}
module yFlagSpacer()
{
  difference()
  {
    cylinder(d=8,h=10);
    translate ([0,0,1]) cylinder(d1=M3hole,d2=6,h=4);
    translate ([0,0,5]) cylinder(d1=6,d2=M3hole,h=4);
    cylinder(d=M3hole,h=10);
  }
}

module yFlagSpacers()
{
  translate([-27, (15/2+4),0]) yFlagSpacer();
  translate([-27,-(15/2+4),0]) yFlagSpacer();
}

module yFlag()
{
  tunning = 4;
  z = 10;
  difference()
  {
    union()
    {
      hull()
      {
        translate([-27, 15/2+4+tunning,z]) cylinder(d=8,h=4);
        if (!lightFlags)
          translate([-27, -(15/2+4+tunning),z]) cylinder(d=8,h=4);
        else
          translate([-27, 15/2+4-tunning,z]) cylinder(d=8,h=4);
      }
      hull()
      {
        translate([-27, 15/2+4+tunning,z]) cylinder(d=8,h=4);
        translate([-27-6/2, 15/2+4+19,z+2]) cube([6,0.01,2]);
      }
      translate([-27-6/2, 15/2+4+19,z+2]) cube([6,6,2]);
    }
    hull()
    {
      translate([-27, 15/2+4+tunning,z-1]) cylinder(d=M3hole,h=7);
      translate([-27, 15/2+4-tunning,z-1]) cylinder(d=M3hole,h=7);
    }
    hull()
    {
      translate([-27, -(15/2+4+tunning),z-1]) cylinder(d=M3hole,h=7);
      translate([-27, -(15/2+4-tunning),z-1]) cylinder(d=M3hole,h=7);
    }
  }
}

module xyR(zPos)
{
  difference()
  {
    union()
    {
      translate([-27/2-20, -35/2, -15.5]) cube([27+1+20,35+5,15.5]);
      //translate([-27/2-20, -23/2-5, -5-8]) cube([20+27,23+5,5+8]);
      hull()
      {
        translate([27/2+1-8, -22+23/2-(mgnPlusY-(idlerPitch)/2), -(29+8+idler2idlerPitch)])
          cube([8,23,29+8]);
        translate([27/2+1-8, -35/2, -15.5-3])
          cube([8,35+5,15.5+3]);
      }
      translate([27/2-15, -22+23/2-(mgnPlusY-(idlerPitch)/2), -(29+8)]) cube([15+1,23,29+8]);
      hull()
      {
        translate([27/2-15-7, -22+23/2-(mgnPlusY-(idlerPitch)/2), -(29+8+idler2idlerPitch)]) 
          cube([15,23,29+8+idler2idlerPitch]);
        translate([-27/2-20, 23/2+(mgnPlusY+(idlerPitch)/2)-1, -(29+9+1+idler2idlerPitch)]) 
          cube([5,7,29+9+1+idler2idlerPitch]);
        
        translate([bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), -29/*-9-1*/])
          cylinder(d=25, h=25.5/*+9+1*/);
        translate([bandAxisDevianceX-(idlerPitch), -(mgnPlusY+(idlerPitch)/2), -(29+9+1+idler2idlerPitch)])
          cylinder(d=25.5, h=29+9+1+idler2idlerPitch);
      }
    }
    XrailHole();
    translate([bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), idler2posZ - zPos])
    { 
      rotate([0,0,90]) idlerHole();
      rotate([0,0,90]) bandHole1();
    }
    translate([bandAxisDevianceX-(idlerPitch), -(mgnPlusY+(idlerPitch)/2), idler1posZ - zPos])  
    {
      rotate([0,0,180]) idlerHole();
      rotate([0,0,180]) bandHole1();
    }
    translate([bandAxisDevianceX+(idlerPitch), -(mgnPlusY-(idlerPitch)/2), idler1posZ - zPos])
    {
      bandHole2();
    }
    xyScrews(-1);
    
    hull()
    {
      translate([-10,10,idler2posZ - zPos -(idlerH+1+0.2)/2-3-50]) 
        cylinder(d=M3head,h=50);
      translate([bandAxisDevianceX, -(mgnPlusY-(idlerPitch)/2), idler2posZ - zPos -(idlerH+1+0.2)/2-3-50])
        cylinder(d=M3head,h=50);
    }
    
    // endstop flag screws
    translate([-27,  (15/2+4),-8]) 
      cylinder(d=M3insertD,h=50);
    if (!lightFlags)
      translate([-27, -(15/2+4),-8]) 
        cylinder(d=M3insertD,h=50);
     
     /*translate([15-1,  -17,-15]) rotate([90,0,90])
     {
       linear_extrude(height = 1)
        text("thingiverse.com", size=4.7);
     }
     translate([15-1,  -15,-21]) rotate([90,0,90])
     {
       linear_extrude(height = 1)
         text("/thing:3786629", size=4.7);
     } 
     translate([15-1,  13,-10]) rotate([90,0,90])
     */
     /*translate([15-1,  9,-22]) rotate([90,0,90])
     {
       linear_extrude(height = 1)
         text("R", size=18);
     }*/ 
     translate([-5, 10, -0.5])
     {
       linear_extrude(height = 1)
         text("R", size=10);
     } 
  }
}

module pipeBandHole()
{
  difference()
  {
    cylinder(d=pipeD+4+5,h=5);
    translate([0,0,-1])
      cylinder(d=pipeD+4,h=10);
  }
}

module pipeEnd()
{
  difference()
  {
    union()
    {
      translate([-50,-28/2,0])
        cube([50,28,5]);
      intersection()
      {
        translate([-15,-30/2,0])
          cube([15,30,40]);
        translate([0,0,-1])
        cylinder(d=pipeD+12, h=50);
      }
      hull()
      {
        translate([-50,6,0])
          cube([50,5,5]);
        translate([-8,6,0])
          cube([8,5,40]);
      }
    }
    translate([0,0,-1])
      cylinder(d=pipeD,h=60);
    translate([0,0,15])
      pipeBandHole();
    translate([0,0,33])
      pipeBandHole();
    translate([-21,0,-1])
      cylinder(d=M5hole,h=10);
    translate([-41,0,-1])
      cylinder(d=M5hole,h=10);
  }
}

module cableCover()
{
  xH1 = 15; //2.54*13;
  xH2 = 15; //2.54*13 - 2;
  yH1 = 5; // 2.54*2
  yH2 = 5; // 2.54*2 - 1;
  
  translate([18,0,0]) difference()
  {
    translate([-(xH1+20)/2,-15-8,-30-8])
      cube([xH1+20,8+10,30+8]);
    translate([-100/2,-15,-30])
      cube([100,30,30]);
    translate([-xH1/2,-15-yH1,-10])
      cube([xH1,yH1+1,11]);
    translate([-(xH2)/2,-15-yH2,-30-8+3])
      cube([xH2,yH1+10,30+10]);

    translate([xH1/2+5,-15-5,-15])
      rotate([90,0,0])
        cylinder(d=M3head,h=10);
    translate([-xH1/2-5,-15-5,-15])
      rotate([90,0,0])
        cylinder(d=M3head,h=10);
    translate([xH1/2+5,-15-5+0.2,-15])
      rotate([-90,0,0])
        cylinder(d=M3hole,h=10);
    translate([-xH1/2-5,-15-5+0.2,-15])
      rotate([-90,0,0])
        cylinder(d=M3hole,h=10);
  }
}

module idlersHolder()
{
  difference()
  {
    union()
    {
      translate([-30/2,-5,mgnYposZ-((28+9+1)+5+13+10+idler2idlerPitch)]) 
        cube([30,5,(28+9+1)+5+13+10+idler2idlerPitch]);
      hull()
      {
        translate([-30/2+7,-5,idler1posZ-idlerH/2-0.5-5]) 
          cube([30-7,5,2*idlerH+1+10+idler2idlerPitch]);
        translate([-30/2+7,-11-idlerDia2/2,idler1posZ-idlerH/2-0.5-5]) 
          cube([1,11+idlerDia2/2,2*idlerH+1+10+idler2idlerPitch]);
        translate([-bandAxisDevianceX,-11,idler1posZ-idlerH/2-0.5-5])
          cylinder (d=idlerDia2, h=2*idlerH+1+10+idler2idlerPitch);
      }
      translate([-16/2,-8,mgnYposZ-(8.5+2)]) cube([16,8,8.5+2]);
      hull()
      {
        translate([0,0,idler1posZ-idlerH/2-10]) rotate([90,0,0]) cylinder(h=8,d=15);
        translate([0,0,idler2posZ+idlerH/2+10]) rotate([90,0,0]) cylinder(h=8,d=15);
      }
      
      // endstop holder
      translate([15-4.5,-15-5,mgnYposZ-13.5]) cube([8,15+5,8]);
      //translate([-30/2,-10,-((28+9+1)+3.5+13)]) cube([30,10,(28+9+1)+3.5+13-20]);
    }
    translate([-12.5/2,-15,mgnYposZ-(8.5)]) cube([12.5,15,8.5]);
    if (idler2idlerPitch >= 2)
    {
      translate([-bandAxisDevianceX,-11,idler1posZ]) idlerHole(screw=0);
      translate([-bandAxisDevianceX,-11,idler2posZ]) idlerHole(screw=0);
      translate([-bandAxisDevianceX,-11,idler1posZ]) rotate([0,0,-90]) idlerHole(screw=0);
      translate([-bandAxisDevianceX,-11,idler2posZ]) rotate([0,0,-90]) idlerHole(screw=0);
      translate([-bandAxisDevianceX,-11,idler2posZ+idlerH/2]) 
        cylinder(d=M3thread,h=7);
      translate([-bandAxisDevianceX,-11,idler1posZ-idlerH/2-10]) 
        cylinder(d=M3hole,h=idler2idlerPitch+2*idlerH+10+1);
      translate([-bandAxisDevianceX,-11,idler1posZ-idlerH/2-10])
        cylinder(d=M3head,h=6);
    }
    else
    {
      translate([-bandAxisDevianceX,-11,bandMiddleZ]) 
        idlerHole(h=2*idlerH+idler2idlerPitch+1);
      translate([-bandAxisDevianceX,-11,bandMiddleZ]) 
        rotate([0,0,-90])
          idlerHole(h=2*idlerH+idler2idlerPitch+1, screw=0);
    }

    translate([0, 0,idler1posZ-idlerH/2-10]) rotate([90,0,0]) cylinder(h=10,d=M5hole);
    translate([0, 0,idler2posZ+idlerH/2+10]) rotate([90,0,0]) cylinder(h=10,d=M5hole);
    translate([0,-5,idler1posZ-idlerH/2-10]) rotate([90,0,0]) cylinder(h=20,d=M5head);
    translate([0,-5,idler2posZ+idlerH/2+10]) rotate([90,0,0]) cylinder(h=20,d=M5head);

    // endstop holder
    translate([15-4.5,-15,mgnYposZ-13.5+4]) rotate([0,90,0]) cylinder(d=M3hole, h=30);
    
    // cable holder
    translate([-2-idlerDia2/2,-11,25]) 
      wireHolder();

  }    
}

module bandTensionerHole()
{
  translate([0,0,-7/2])
  {
    hull()
    {
      translate([30-10/2-2.5,0,0]) cylinder(d=10,h=7);
      translate([7,-10/2,0]) cube([1,10,7]);
    }
    translate([8,0,7/2]) rotate([0,-90,0]) cylinder(d=M3insertD,h=10);
    translate([-7,-10/2,0]) cube([15,2,7]);
    translate([-7,+10/2-1,0]) cube([15,1,7]);
    for (i = [0 : 5])
    {
      translate([-5+i*2,+10/2-2,0]) cube([1,1,7]);
    }
  }
}



if (details >= 1)
{
  color("silver") translate([-450/2,-410/2,mgnYposZ]) rotate([180,0,90]) render() mgn12rail(410,15);
  color("silver") translate([450/2,-410/2,mgnYposZ]) rotate([180,0,90]) render() mgn12rail(410,15);
}

/* glass */
//if (teardown < 1) translate([-50,-70,-4-42.3/2-24]) color("gray", 0.1) cube([100,100,4]);

module idler()
{
  render()
  difference () {
    union () {
      translate([0,0,-idlerH/2]) cylinder(d=idlerDia2,h=idlerHw);
      translate([0,0,-idlerH/2]) cylinder(d=idlerDia,h=idlerH);
      translate([0,0,idlerH/2-idlerHw]) cylinder(d=idlerDia2,h=idlerHw);
    }
    translate([0,0,-idlerH/2-1]) cylinder(d=M3hole,h=idlerH+2);
  }
}

// source https://www.thingiverse.com/thing:2784911
//translate([headX-28,-30+headY,0]) rotate([0,0,180]) import ("BLTouch_Model.stl");
module blTouch()
{
  //translate(bltouchPos) rotate([0,0,90]) import ("BLTouch_Model.stl");
  translate(bltouchPos) rotate([0,0,90]) import ("BLTouch_Model.stl");
}

module endStop()
{
  // https://grabcad.com/library/opto-endstop-tcst2103-1
  import("Endstop_Opto_TCST2103.stl");
}

module blTouchPoint()
{
  if  (BMG_Aero)
  {
    echo ("BLtouch offset", bltouchPosBMGaero - nozzlePosBMGaero - [0,0,blTouchTouch]);
    // noozle
    translate(nozzlePosBMGaero + [0,0,-20])
      cylinder(d1=5,d2=0.5,h=20);
    translate(nozzlePosBMGaero + [0,0,-0.1+19])
      cylinder(d=10,h=0.1);
    translate(nozzlePosBMGaero + [0,0,-0.1])
      cylinder(d=3,h=0.1);
  }
  else
  {
    echo ("BLtouch offset", bltouchPosTitan - nozzlePosTitan - [0,0,blTouchTouch]);
    // noozle
    translate(nozzlePosTitan + [0,0,-20])
      cylinder(d1=5,d2=0.5,h=20);
    translate(nozzlePosTitan + [0,0,-0.1+19])
      cylinder(d=10,h=0.1);
    translate(nozzlePosTitan + [0,0,-0.1])
      cylinder(d=3,h=0.1);
  }
  // touch
  translate(bltouchPos + [0,0,-20 - blTouchTouch ])
    cylinder(d1=5,d2=0.5,h=20);
  translate(bltouchPos + [0,0,-40.3])
    cylinder(d=5,h=0.1);
  translate(bltouchPos + [0,0,-45.7])
    cylinder(d=5,h=0.1);
  translate(bltouchPos + [0,0,-46.6])
    cylinder(d=5,h=0.1);
  
}


if (details >= 2)
{
  color("silver", 1)
  {
    // idlers
    translate([bandAxisX,410/2-11,idler1posZ]) idler();
    translate([bandAxisX,410/2-11,idler2posZ]) idler();
    translate([-bandAxisX,410/2-11,idler1posZ]) idler();
    translate([-bandAxisX,410/2-11,idler2posZ]) idler();

    translate([-bandAxisX,-410/2+42.3/2,idler2posZ]) idler(); // left motor
    translate([bandAxisX,-410/2+42.3/2,idler1posZ]) idler(); // right motor

    translate([-bandAxisX,idlerPitch/2+headY,idler1posZ]) idler(); //left xy bottom
    translate([-bandAxisX+idlerPitch,-idlerPitch/2+headY,idler2posZ]) idler(); //left xy top

    translate([bandAxisX,idlerPitch/2+headY,idler2posZ]) idler(); //right xy top
    translate([bandAxisX-idlerPitch-2,-idlerPitch/2+headY,idler1posZ]) idler(); //right xy bottom
  }
}

if (show)
{
  if (details >= 2)
  {
    color("black", 1)
    {
      //band (back X)
      translate([-bandAxisX,410/2-11+idlerDia/2,idler1posZ-bandW/2]) cube([2*bandAxisX,2,bandW]);
      translate([-bandAxisX,410/2-11+idlerDia/2,idler2posZ-bandW/2]) cube([2*bandAxisX,2,bandW]);

      //band (left Y outer)
      translate([-bandAxisX-idlerDia/2-2,-410/2+42.3/2,idler2posZ-bandW/2]) cube([2,(410/2-11)-(-410/2+42.3/2),bandW]);
      //band (left Y inner front)
      translate([-bandAxisX+idlerDia/2-2/2,-410/2+42.3/2,idler2posZ-bandW/2]) cube([2,(-idlerDia/2-1+headY)-(-410/2+42.3/2),bandW]);
      //band (left Y inner back)
      translate([-bandAxisX-idlerDia/2-2,+idlerDia/2-1+headY,idler1posZ-bandW/2]) cube([2,(410/2-11)-(+idlerDia/2-1+headY),bandW]);

      //band (right Y)
      translate([bandAxisX+idlerDia/2,-410/2+42.3/2,idler1posZ-bandW/2]) cube([2,(410/2-11)-(-410/2+42.3/2),bandW]);
      //band (right Y inner front)
      translate([bandAxisX-idlerDia/2-2+2/2,-410/2+42.3/2,idler1posZ-bandW/2]) cube([2,(-idlerDia/2-1+headY)-(-410/2+42.3/2),bandW]);
      //band (right Y inner back)
      translate([bandAxisX+idlerDia/2,+idlerDia/2-1+headY,idler2posZ-bandW/2]) cube([2,(410/2-11)-(+idlerDia/2-1+headY),bandW]);

      //band X left upper
      translate([-bandAxisX+idlerDia,headY-1,idler2posZ-bandW/2]) cube([headX+bandAxisX-idlerDia-15,2,bandW]);
      //band X left lower
      translate([-bandAxisX,headY-1,idler1posZ-bandW/2]) cube([headX+bandAxisX-15,2,bandW]);
      //band X right upper
      translate([headX+15,headY-1,idler2posZ-bandW/2]) cube([-headX+bandAxisX-15,2,bandW]);
      //band X right lower
      translate([headX+15,headY-1,idler1posZ-bandW/2]) cube([-headX+bandAxisX-idlerDia-15,2,bandW]);
    }
  }
  else
  {
    color("black", 1)
    {
      //band X left upper
      translate([headX-15-45,headY-1,idler2posZ-bandW/2]) cube([45,2,bandW]);
      //band X left lower
      translate([headX-15-45,headY-1,idler1posZ-bandW/2]) cube([45,2,bandW]);
      //band X right upper
      translate([headX+15,headY-1,idler2posZ-bandW/2]) cube([45,2,bandW]);
      //band X right lower
      translate([headX+15,headY-1,idler1posZ-bandW/2]) cube([45,2,bandW]);
    }
  }  
}

module motorHolder(hTop,hardStop=false, left=false)
{
  xFrameCenter = -42.3/2 + 30/2;
  difference()
  {
    union()
    {
      hull()
      {
        //translate([-42.3/2,-(42.3/2+30),0]) cube([42.3,30+5,20]);
        translate([ (42.3/2-5), (42.3/2-5),0]) cylinder(d=10,h=6);
        translate([-(42.3/2-5), (42.3/2-5),0]) cylinder(d=10,h=6);
        translate([ (42.3/2-5),-(42.3/2),0]) cylinder(d=10,h=20);
        hull()
        {
          translate([-(42.3/2-5),-(42.3/2),20-5])sphere(d=10);
          translate([-(42.3/2-5),-(42.3/2),0]) cylinder(d=10,h=2);
        }
        hull()
        {
          translate([-(42.3/2-10),-(42.3/2)-30+2.5,20-5])sphere(d=5);
          translate([-(42.3/2-10),-(42.3/2)-30+2.5,0]) cylinder(d=5,h=2);
        }
      }
      translate([42.3/2-30-5,-42.3/2-30,0]) cube([35,35,hTop]);

      // emergency hard stop
      if (hardStop) 
      {
        /*hull()
        {
          translate([31/2-6/2,-25/2+15,0]) cube([6,25-15-0.3,30]);
          translate([31/2,-25/2+15,0]) cylinder(d=6,h=30);
          translate([31/2,-25/2,0]) cylinder(d=6,h=13);
        }*/
        translate([-31/2,31/2,0]) cylinder(d=31-20,h=15);
        translate([-31/2-8/2,31/2-31,0]) cube([8,31,15]);
      }
      
      
      hull()
      {
        translate([-xFrameCenter,-42.3/2+5,hTop-9-10]) rotate([-90,0,0]) cylinder(d1=11+6,d2=11+3,h=2);
        translate([-xFrameCenter,-42.3/2+5,(11+6)/2]) rotate([-90,0,0]) cylinder(d1=11+6,d2=11+3,h=2);
      }
      hull()
      {
        translate([-xFrameCenter-30/2-5,-42.3/2-30/2,hTop-10-5]) rotate([0,-90,0]) cylinder(d1=11+6,d2=11+3,h=2);
        translate([-xFrameCenter-30/2-5,-42.3/2-30/2,10]) rotate([0,-90,0]) cylinder(d1=11+6,d2=11+3,h=2);
      }
    }
    translate([42.3/2-30,-42.3/2-30,0]) cube([30,30,100]);
    
    translate([xFrameCenter+12.5/2,-50,hTop-8.5]) cube([12.5,50,8.5]);
    
    cylinder(d=motorFiletD,h=2.5);
    translate([0,0,2.5]) cylinder(d1=motorFiletD,d2=20,h=2);
    cylinder(d=20,h=100);
    
    translate([-20/2,0,5]) cube([20,100,50]);
    
    translate([-xFrameCenter,-50,hTop-9-10]) rotate([-90,0,0]) cylinder(d=M5hole,h=50);
    translate([-xFrameCenter,-42.3/2+5,hTop-9-10]) rotate([-90,0,0]) cylinder(d=M5head,h=5);

    translate([0,-42.3/2-30/2,hTop-10-5]) rotate([0,-90,0]) cylinder(d=M5hole,h=50);
    translate([-xFrameCenter-30/2-5,-42.3/2-30/2,hTop-10-5]) rotate([0,-90,0]) cylinder(d=M5head,h=5);
    translate([0,-42.3/2-30/2,10]) rotate([0,-90,0]) cylinder(d=M5hole,h=50);
    translate([-xFrameCenter-30/2-5,-42.3/2-30/2,10]) rotate([0,-90,0]) cylinder(d=M5head,h=5);
    
    translate([31/2,31/2,-1]) cylinder(d=M3hole,h=30);
    translate([-31/2,31/2,-1]) cylinder(d=M3hole,h=30);
    translate([31/2,-31/2,-1]) cylinder(d=M3hole,h=30);
    translate([-31/2,-31/2,-1]) cylinder(d=M3hole,h=30);

    translate([31/2,31/2,5]) cylinder(d=M3head,h=20);
    translate([-31/2,31/2,5]) cylinder(d=M3head,h=20);
    translate([31/2,-31/2,10]) cylinder(d=M3head,h=20);
    translate([-31/2,-31/2,10]) cylinder(d=M3head,h=20);
    translate([31/2,-31/2,10+20]) cylinder(d1=M3head,d2=1,h=5);
    translate([-31/2,-31/2,10+20]) cylinder(d1=M3head,d2=1,h=5);

    if (left)
    {
      translate([11, -42.3/2+0.5,5]) mirror([1,0,0]) rotate([90,0,0])
      {
        linear_extrude(height = 1)
          text("L", size=12);
      }
    }
    else
    {
      translate([0, -42.3/2+0.5,5]) rotate([90,0,0])
      {
        linear_extrude(height = 1)
          text("R", size=12);
      }
    } 
    translate([-17,-42.3/2-30/2,((hTop-10-5)+(10))/2 - 3.4/2])
      wireHolder();
  }
}

module fan()
{
  rotate([0,0,fanRotate])
  import ("5015_Blower_Fan.stl"); // https://www.thingiverse.com/thing:1576438
}
module fanConnector()
{
  translate([0,-1,-15]) cube([19.5,1,15]);
}

module xAxisComplet()
{
  color("silver") translate([Yrail/2,mgnPlusY,mgnXposZ]) rotate([0,0,180]) render() mgn15rail(Yrail,10);
  if (details >= 1)
  {
    translate([-450/2,mgnPlusY,mgnYposZ-13]) 
    {
      color("lightgreen",0.9) render() xyL(zPos=mgnYposZ-13);
      //color("black",0.3)
      color("orange",0.6)
        translate([0,0,0.01])  render() xFlag();
    }
    translate([450/2,mgnPlusY,mgnYposZ-13]) 
    {
      color("lightgreen",0.9) render() xyR(zPos=mgnYposZ-13);
      //color("black",0.3)
      color("orange",0.6)
        translate([0,0,0.02]) render() yFlag();
      color("purple",0.4)
        translate([0,0,0.01]) render() yFlagSpacers();
    }
    color("green") render() translate([-450/2,mgnPlusY,mgnYposZ]) rotate([180,0,90]) mgn12h();
    color("green") render() translate([450/2,mgnPlusY,mgnYposZ]) rotate([180,0,90]) mgn12h();
  }
}

module wireHolder(whd=6)
{
  difference()
  {
   cylinder(d=whd+4,h=3.4);
   translate([0,0,-1]) 
     cylinder(d=whd,h=4+2);
  }
}

module fanRing()
{
  tr = [-16.5,-11,-13.601];
  translate(tr)
      jets([20-13-5,0,0]-tr);
}

module fanSide(fanWall)
{
  hull()
  {
    translate([-fanWall,-5+1.8+2*fanWall,-15-fanWall])
      cube ([20+2*fanWall,0.01,15+2*fanWall]);
    translate([-fanWall,-14-30+1.8+1,-15-fanWall])
      cube ([20+2*fanWall,0.01,15+2*fanWall]);
    *translate([-fanWall,-14-50/2-20+1.8,fanWall-50/2])
      cube ([20+2*fanWall+5,15+2*fanWall+20,0.01]);
    translate([-fanWall,-14-50/2-0.2+1.8,-5-1.6-50/2-6])  // R
      cube ([20+2*fanWall+14.2,10+2*fanWall+2,0.1]);

  }
}

module fanOutput()
{
  translate([fanXoffset,fanYoffset+0.01,fanZoffset])
    rotate([90,0,180]) 
      {
        intersection()
        {
          rotate([0,0,fanRotate]) 
            translate([0,-20,-15])
              cube ([22,30,15]);
            translate([0,-50,-15])
              cube ([20,100,15]);
        }
        rotate([0,0,fanRotate]) 
         translate([0,-0.01,-15])
           cube ([22,20,15]);
        rotate([90,0,fanRotate])
         translate([0.5,-0.01-15+6.5,-5])
           cylinder(d=3,h=30);
        rotate([90,0,0])
         translate([10,-0.01-15+6.5,-15])
           rotate([0,90,0]) cylinder(d=3,h=20);
        translate([0,-19+3.01,-15])
          cube ([20,19,15]);
        rotate([0,0,fanRotate]) 
         translate([-2,3,-25])
           cube ([30,30,40]);
        intersection()
        {
          translate([0,-14-2+1.8,-50/2+fanWall-0.01])
            rotate([0,90,0])
              difference()
              {
                intersection()
                {
                  cylinder(d=50-2*fanWall,h=20+10);
                  translate([-60,-60,-1]) cube(60);
                }
                translate([0,0,-1]) cylinder(r=50/2-(15+fanWall),h=60);
              }
          fanSide(0);
        }
        intersection()
        {
          translate([0,-19+3.01,-15])
            cube ([20+10,19,15]);
          fanSide(0);
        }
        *intersection()
        {
          hull()
          {
            translate([0,-14-2-50/2+fanWall+1.8,-50/2+fanWall])
              cube ([20+10,15,0.01]);
            translate([0,-14-2-50/2-0.2+fanWall+1.8,-5-fanWall-50/2-6+1]) // R
              cube ([20+20,9.8,0.01]);
          }
          fanSide(0);
        }
      }
}

tr = [-16.5,-11,-13.601];
connTr = -motor2absTransform - [0,0,-28] -tr;
module fanConnA()
{
  m = [20-13-5,0,0] - tr;
  
  render() difference()
  {
      union()
      {
          difference()
          {
            union()
            {
              translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
                rotate([90,0,180])
                  union()
                  {
                    translate([-fanWall,-14+1.8,-15-fanWall])
                      cube ([20+2*fanWall,14+13+fanWall,15+2*fanWall]);
                    intersection()
                    {
                      union()
                      {
                        translate([-fanWall,-14+1.8-0.02,-15-fanWall])
                          cube ([20+2*fanWall+10,14+13+fanWall,15+2*fanWall]);
                        translate([-fanWall,-14+1.8,-50/2+fanWall])
                        {
                          rotate([0,90,0])
                            difference()
                            {
                              intersection()
                              {
                                cylinder(d=50,h=20+2*fanWall+10/*,$fn=128*/);
                                translate([-60,-60,-1]) cube(60);
                              }
                              translate([0,0,-1]) cylinder(r=50/2-(15+2*fanWall),h=60);
                            }
                        }
                        *hull()
                        {
                          translate([-fanWall,-14-50/2+1.8,fanWall-50/2])
                            cube ([20+2*fanWall+10,15+2*fanWall,0.01]);
                          translate([-fanWall,-14-50/2-0.2+1.8-1.6,-5-fanWall-50/2-6]) // R
                            cube ([20+2*fanWall+20,9.8+2*fanWall,0.01]);
                        }
                      }
                      fanSide(fanWall);
                    }
                    //hull()
                    {
                      intersection()
                      {
                        translate([-fanWall,-14+1.8,-50/2+fanWall])
                          rotate([fanCutAngle+90,0,0])
                            translate([-2,3,-2])
                              cube([40,24,4]);
                          fanSide(fanWall+2);
                      }
                      translate([-fanWall,-14+1.8,-50/2+fanWall])
                        rotate([fanCutAngle+90,0,0])
                          hull()
                          {
                            translate([-4,4+23/2,-2-2])
                              cylinder(d=10,h=2+6+2);
                            translate([33,4+23/2,-2-2])
                              cylinder(d=10,h=2+6+2);
                          }
                    }
                  }
              *translate(Cdiff+[0,0,0])
                fanductO(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
        /*
              *hull()
              {
                translate(-connTr+Cdiff+[29,-18,8.6])
                  cylinder(d=10,h=5);
                translate(-connTr+Cdiff+[0,0,8.6])
                  cylinder(d=10,h=5);
              }
              *hull()
              {
                translate(-connTr+Cdiff+[29,25,8.6])
                  cylinder(d=10,h=5);
                translate(-connTr+Cdiff+[0,0,8.6])
                  cylinder(d=10,h=5);
              }
        */
            }
              gap = [0.16, 0.4, 0.16]; //XYZ
              translate(motor2absTransform + [-gap[0],gap[1],-gap[2]])
                render() 
                  back(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
              translate(motor2absTransform + [+gap[0],gap[1],-gap[2]])
                render() 
                  back(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
          }
          translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
            rotate([90,0,180])
              translate([-fanWall+6.7,-14+1.8-10,-15-fanWall-8])
                cube ([6,27,15+2*fanWall]);
        }

//    tr = [-16.5,-11,-13.601];
//    connTr = -motor2absTransform - [0,0,-28] -tr;
    //m = [20-13-5,0,0] - tr;
    //translate(-connTr+Cdiff+[0,0,15-motorAxisZ-2]) 
      //jetsInner(m);
            fanOutput();
    translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
      rotate([90,0,180])
      {
        translate([-fanWall-10,-14+1.8,-50/2+fanWall])
          rotate([fanCutAngle+90,0,-0.02])
            translate([-3,-10,0])
              cube([60,40,17.04]);

        translate([-fanWall,-14+1.8,-50/2+fanWall])
          rotate([fanCutAngle+90,0,0])
            union()
            {
              translate([-4,4+23/2,-2-2-1])
                cylinder(d=M3hole,h=4+2);
              translate([33,4+23/2,-2-2-1])
                cylinder(d=M3hole,h=4+2);
//              translate([-4,4+23/2,-2-2-1])
//                cylinder(d=M3head,h=3);
//              translate([33,4+23/2,-2-2-1])
//                cylinder(d=M3head,h=3);
//              translate([-4,4+23/2,0])
//                cylinder(d=M3insertD,h=M3insertH);
//              translate([33,4+23/2,0])
//                cylinder(d=M3insertD,h=M3insertH);
            }
      }
    translate(/*-motor2absTransform+*/[10,11,9])
      rotate([0,-90,0])
      {
        cylinder (d=M3hole,h=30);
        translate([0,0,20]) cylinder (d=M3head,h=30);
        //cylinder (d=M3head,h=11);
      }

    }
//  tr = [-16.5,-11,-13.601];
}

module fanConnB()
{
  m = [20-13-5,0,0] - tr;
  difference()
  {
    union()
    {
      translate(Cdiff+[0,0,2])
        fanductO(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
      translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
        rotate([90,0,180])
        {
          hull()
          {
            intersection()
            {
              translate([-fanWall,-14+1.8,-50/2+fanWall])
                rotate([fanCutAngle+90,0,0])
                  translate([-2,3,0])
                    cube([40,24,2]);
                fanSide(fanWall+2);
            }
            *translate([-fanWall,-14+1.8,-50/2+fanWall])
              rotate([fanCutAngle+90,0,0])
              {
                translate([-4,4+23/2,0])
                  cylinder(d=11,h=4);
                translate([33,4+23/2,0])
                  cylinder(d=11,h=4);
              }

          }
          render() translate([-fanWall,-14+1.8,-50/2+fanWall])
            rotate([fanCutAngle+90,0,0])
              hull()
              {
                translate([-4,4+23/2,0])
                  cylinder(d=11,h=6);
                translate([33,4+23/2,0])
                  cylinder(d=11,h=6);
                translate([-4,4+23/2,1.2])
                  cylinder(d=8,h=6);
                translate([33,4+23/2,1.2])
                  cylinder(d=8,h=6);
              }
        }
        hull()
        {
          translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
          rotate([90,0,180])
          intersection() // prvni konec spojky
          {
            translate([-fanWall,-14+1.8,-50/2+fanWall])
            {
              rotate([0,90,0])
                difference()
                {
                  intersection()
                  {
                    cylinder(d=50,h=20+2*fanWall+10/*,$fn=128*/);
                    translate([-60,-60,-1]) cube(60);
                  }
                  translate([0,0,-1]) cylinder(r=50/2-(15+2*fanWall),h=60);
                }
            }
            fanSide(fanWall);
            translate([-fanWall,-14+1.8,-50/2+fanWall])
              rotate([fanCutAngle+90,0,0])
                translate([-2,4,0])
                  cube([40,23,0.01]);
          }
          intersection() // druhy konec spojky
          {
            if (BMG_Aero)
              translate([-40,-4,-mgnXposZ])
                cube([60,0.3,50]);
            else
              translate([-40,-11,-mgnXposZ])
                cube([60,0.3,50]);
            translate(Cdiff+[0,0,2])
              fanductO(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
          }
        }
    }

    tr = [-16.5,-11,-13.601];
    connTr = -motor2absTransform - [0,0,-28] -tr;
    m = [20-13-5,0,0] - tr;
    translate(-connTr+Cdiff+[0,0,15-motorAxisZ-2+0.5]) 
      jetsInner(m);
    hull() // dutina spojky
    {
      translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
      rotate([90,0,180])
      intersection() // prvni konec spojky
      {
        translate([-fanWall,-14+1.8,-50/2+fanWall])
        {
          rotate([0,90,0])
            difference()
            {
              intersection()
              {
                cylinder(d=50-2*fanWall,h=20+10/*,$fn=128*/);
                translate([-60,-60,-1]) cube(60);
              }
              translate([0,0,-1]) cylinder(r=50/2-(15+fanWall),h=60);
            }
        }
        fanSide(0);
        translate([-fanWall,-14+1.8,-50/2+fanWall])
          rotate([fanCutAngle+90,0,0])
            translate([-2,4,-0.11])
              cube([40,23,0.01]);
      }
      intersection() // druhy konec spojky
      {
        if (BMG_Aero)
          translate([-40,-4-0.1,-mgnXposZ])
            cube([60,0.4,50]);
        else
          translate([-40,-11,-mgnXposZ])
            cube([60,0.3,50]);
        translate(-connTr+Cdiff+[0,0,15-motorAxisZ-2+0.5]) 
          jetsInner(m);
      }
    }
    translate([fanXoffset,fanYoffset+0.01,fanZoffset-2])
    rotate([90,0,180])
    {
      translate([-fanWall,-14+1.8,-50/2+fanWall])
        rotate([fanCutAngle+90,0,0])
          union()
          {
//          translate([-4,4+23/2,-2-2-1])
//            cylinder(d=M3hole,h=4+2);
//          translate([33,4+23/2,-2-2-1])
//            cylinder(d=M3hole,h=4+2);
//              translate([-4,4+23/2,-2-2-1])
//                cylinder(d=M3head,h=3);
//              translate([33,4+23/2,-2-2-1])
//                cylinder(d=M3head,h=3);
            translate([-4,4+23/2,-1])
              cylinder(d=M3insertD,h=M3insertH+1+0.5);
            translate([33,4+23/2,-1])
              cylinder(d=M3insertD,h=M3insertH+1+0.5);
          }
        // enhance space for bltouch
        translate([-2.1,-40,-23]) rotate([0,180+30,0]) cube(20);
    }
  }
}

module fanJetHolder()
{
  pos = -connTr+Cdiff+ ((BMG_Aero)?[0,0,2] : [0,0,0]);
  difference()
  {
    union()
    {
      hull()
      {
        translate(pos+[-3.3,-21,23.9+3-1.8])
          rotate([-90,0,0])
            cylinder(d=6,h=4,$fn=16);
        translate(pos+[-3.3+12,-21,23.9+3-1.8])
          rotate([-90,0,0])
            cylinder(d=6,h=4,$fn=16);
      }
      hull()
      {
        translate(pos+[-3.3,-21,23.9+3-1.8])
          rotate([-90,0,0])
            cylinder(d=6,h=4,$fn=16);
        translate(pos+[-7-12,-21,23.9-10])
          cube([6,4,2]);
      }
      hull()
      {
        translate(pos+[-3.3+12,-21,23.9+3-1.8])
          rotate([-90,0,0])
            cylinder(d=6,h=4,$fn=16);
        translate(pos+[-7+(20-6)+6,-21,23.9-10])
          cube([6,4,2]);
      }
      hull()
      {
        translate(pos+[-7-12,-21,23.9-10.3])
          cube([6,4,3]);
        translate(pos+[-7-12+4,-21-4,23.9-10.3])
          cube([6,4,2]);
      }
      hull()
      {
        translate(pos+[-7+(20-6)+6,-21,23.9-10.3])
          cube([6,4,3]);
        translate(pos+[-7+(20-6)+6-4,-21-4,23.9-10.3])
          cube([6,4,2]);
      }
    }
    translate(pos+[-3.3,-21+5,23.9+3-1.8])
      rotate([90,0,0])
        cylinder(d=M2hole,h=10,$fn=16);
    translate(pos+[-3.3+12,-21+5,23.9+3-1.8])
      rotate([90,0,0])
        cylinder(d=M2hole,h=10,$fn=16);
  }
}

module headComplet()
{
  color("green") render() translate([0,mgnPlusY,mgnXposZ]) rotate([0,0,0]) mgn15h();
  translate([0,0.01,0])
    color("lightgreen",0.6) 
      render() 
        tensioner(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
  /*
  translate([0,0.01,0])
    color("orange",0.6)
      render() 
        fanSpacer(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY); */
  color("orange",0.6) 
  {
    render() 
      tensionerClampL(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    render() 
      tensionerClampR(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
  }
  color("Chocolate",0.6) 
    render()
      airDeflector(topL=-15+16+mgnXposZ+(15 - motorAxisZ),offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);

  translate(motor2absTransform) 
  {
    color("orange",0.6) 
      render() 
        front(topL=-15+16+mgnXposZ+(15 - motorAxisZ),offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    color("orange",0.6) 
      render() 
        backFanDish(topL=-15+16+mgnXposZ+(15 - motorAxisZ),offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    color("purple",0.6) 
      render() 
        back(topL=-15+16+mgnXposZ+(15 - motorAxisZ),offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    if (cableHolder2)
      translate([0,0.01,0])
        color("lightgreen",0.6) 
          render() 
            cableHolder(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY-7.5);
  }
  
  if (showExtruder) translate(motor2absTransform) rotate([0,0,-90]) 
  {
    //rotate([0,-90,90])  
    //{
    //  color("gold", 0.4) translate([0,0,-motorBodyLen-0.01]) rotate([0,180,0]) render() //cooler_404011();
    //}
    translate([0,motorWall-3,0]) rotate([0,-90,90]) color("gray", 0.4) render() nema17();
    if (BMG_Aero)
    {
      color("gray", 0.6) 
        translate([0, 0, 3.01]) 
          BMG_Aero_model();
    }
    else
    {
      translate([0,-2*teardown,0]) rotate([0,-90,90]) 
        color("gray", 0.6) 
          translate([0, 0, 3.01]) titan();
    }
    //rotate([0,-90,90]) translate([-25,-11.5,12+2]) rotate ([0,-90,0]) color("silver",0.4) render () coller();
  }

  color("red",0.5) blTouchPoint();
  if (showBLtouch) color("white", 0.6) blTouch();
  color("white", 0.6)
    translate([-35.5-0.01,mgnPlusY,68+0.01])
      rotate([180,0,90])
        endStop();
  if (showFan)
    color("silver", 0.6) 
      translate([fanXoffset,fanYoffset+0.01,fanZoffset]) 
        rotate([90,0,180]) 
          fan();
  color("lightgreen",0.6) 
    render() 
    {
      fanConnB();
      fanJetHolder();
    }
  color("orange",0.6) render() fanConnA();
  
  /*color("blue",0.8) render() fanJetHolder(); */
  if (conn == "pipe")
    translate([-25,mgnPlusY,mgnXposZ+15+9+0.01]) 
      color("lightgreen",0.6) 
        render()
          rotate([0,0,180])
            pipeHeadHolder();
  if (conn == "chain")
    translate([0,mgnPlusY,mgnXposZ+15+9+0.01]) 
      color("lightgreen",0.6) 
        render() 
          chainHeadHolder();
  *translate([-15,mgnPlusY+8,mgnXposZ+25+34]) 
    color("gray",0.6) 
      render() 
        rotate([-90,0,0])
          pcbMockup();
  if(showpipe)
    translate([0,mgnPlusY,mgnXposZ+15+9+0.01]) 
      color("gray",0.6) 
        render() 
          pipe();
  if (conn == "chain")
    translate([0,mgnPlusY,mgnXposZ+15+9+0.01]) 
      color("gray",0.6) 
        render() 
          chain();

  if (BMG_Aero)
  {
    tr = [-16.5,-11,-13.601];
    connTr = -motor2absTransform - [0,0,-28] -tr;
*    color("blue",0.6) 
      render() 
        translate(-connTr+Cdiff)
          completeFanDuct();
  }
  else
  {
    tr = [-16.5,-11,-13.601];
    connTr = -motor2absTransform - [0,0,-28] -tr;
    *color("blue",0.6) 
      render() 
        translate(-connTr)
          completeFanDuct();
        //fanductO(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
//    color("blue",0.5) 
//    translate([headXoffset-rodOffset,-35-mgnPlusY,-23])
//    translate(motor2absTransform + [0,0,-28])
//      completeFanDuct();
//      fanRing();
  }
}
module Zsetup(bedPos)
{
  if (z3in1)
  {
    translate([-420/2+22.01,0,frameOffsetZ-110]) color("purple",0.8) render() top3in1(pitch = 120-12);
  }
  else
  {
    translate([-420/2+22.01,0,frameOffsetZ-110]) color("purple",0.8) render() top();
    translate([-420/2+22.01,0,frameOffsetZ-110]) 
      color("purple",0.8) 
        render() 
          translate([0,-(120-12),0.02]) 
            topRod();
    translate([-420/2+22.01,0,frameOffsetZ-110]) 
      color("purple",0.8) 
        mirror([0,1,0])
          render() 
            translate([0,-(120-12),0.02]) 
              topRod();
  }

  translate([-420/2+22.01,0,frameOffsetZ-110])
  {
    translate([0,-(120-12),12]) color("silver", 0.4) rotate([180,0,0]) cylinder(d=12, h=400);
    mirror([0,1,0]) translate([0,-(120-12),12]) color("silver", 0.4) rotate([180,0,0]) cylinder(d=12, h=400);
  }
  
  translate([-420/2+22.01,0,frameOffsetZ-440+0.01]) color("purple",0.8) render() bottom();
  translate([-420/2+22.01,0,frameOffsetZ-440+0.01])
  {
    color("purple",0.8) mirror([0,0,1]) render() translate([0,-(120-12),30.02]) topRod(-2);
    color("purple",0.8) mirror([0,1,0]) mirror([0,0,1]) render() translate([0,-(120-12),30.02]) topRod(-2);
    translate([0,0,-7]) color("gray", 0.4) render() nema17_T8(40,350);
  }
  translate([-420/2+22.01,0,frameOffsetZ-110+0.01]) 
  {
    //tableCorner(-(120-12), +bedPos);
    //mirror([0,1,0]) tableCorner(-(120-12), +bedPos);
    translate([27+0.01,-120,-20-0.01+bedPos]) 
      color("silver", 0.4) 
        cube([20,240,20]);
    if (z3in1)
    {
      translate([0,0,bedPos]) color("lightgreen",0.8) render() zMiddle3in1(120-12);
    }
    else
    {
      translate([0,-(120-12),+bedPos]) 
        color("lightgreen",0.8) render() frameCorner();
      mirror([0,1,0]) translate([0,-(120-12),+bedPos]) 
        color("lightgreen",0.8) render() frameCorner();
      translate([0,0,+bedPos]) color("lightgreen",0.8) render() middle();
    }
    color("gray",0.6) 
      translate([0,-(120-12),bedPos-57+10-1]) cylinder(d=21, h=57);
    color("gray",0.6) mirror([0,1,0]) 
      translate([0,-(120-12),bedPos-57+10-1]) cylinder(d=21, h=57);
  }
}

module bedHolders(bedPos)
{
  translate([-310/2,-310/2+15,frameOffsetZ-110+0.01+bedPos])
  {
    color("lightgreen",0.8) render () mirror([1,0,0]) bedHolder(15);
    color("orange",0.8) translate([0,-15,-10-18-7+5]) render() blNut();
  }
  mirror([0,1,0])
    translate([-310/2,-310/2+15,frameOffsetZ-110+0.01+bedPos])
      {
        color("lightgreen",0.8) render() mirror([1,0,0]) bedHolder(15);
        color("orange",0.8) translate([0,-15,-10-18-7+5]) render() blNut();
      }
}

module bedX2020(bedPos)
{
  translate([-415/2,120.01,frameOffsetZ-110+bedPos-20])
    color("silver", 0.4) 
      cube([415,20,20]);
}

if (show)
{

  if (cornerTest)
  {
    translate([headX_L,headY_F,0]) headComplet();
    translate([headX_R,headY_F,0]) headComplet();
    translate([headX_L,headY_R,0]) headComplet();
    translate([headX_R,headY_R,0]) headComplet();

    translate([0,headY_F,0]) xAxisComplet();
    translate([0,headY_R,0]) xAxisComplet();
  }
  else
  {
    translate([headX,headY,0]) headComplet();  
    translate([0,headY,0]) xAxisComplet();
  }
    
  if (details >= 1)
  {
    translate([-450/2,410/2,0]) color("lightgreen",0.9) render() idlersHolder();
    
    if (conn == "pipe")
      translate([-10,410/2+30/2,mgnYposZ+30]) 
        color("lightgreen",0.9) 
          render() 
            pipeEnd();

    translate([-10,410/2+30/2,mgnYposZ+30]) color("orange",0.9) render() cableCover();

    mirror([1,0,0]) translate([-450/2,410/2,0]) color("lightgreen",0.9) render() idlersHolder();

    // right motor holder
    translate([bandAxisX,-410/2+42.3/2+0.01,xyMotor1PosZ]) color("lightgreen",0.9) render() motorHolder(frameOffsetZ-xyMotor1PosZ,false,left=false);
    // left motor holder
    translate([-bandAxisX,-410/2+42.3/2+0.01,xyMotor2PosZ]) color("lightgreen",0.9) mirror([1,0,0]) render() motorHolder(frameOffsetZ-xyMotor2PosZ,true,left=true);
    
    color("white", 0.6)
      translate([450/2,410/2,mgnYposZ]+[-19.5,-15,0])
        rotate([0,90,180])
          endStop();

  }

  if (details >= 4)
  {
    //translate([0,bedYoffset,0])
    {
      // bed on Top
      bedX2020(bedTop);
      mirror([0,1,0]) bedX2020(bedTop);
      Zsetup(bedTop);
      mirror([1,0,0]) Zsetup(bedTop);
      translate([bedXoffset,0,0]) 
      {
        bedHolders(bedTop);
        mirror([1,0,0]) bedHolders(bedTop);
      }
      color("silver", 0.4) 
        translate([324/-2+bedXoffset,324/-2,frameOffsetZ-110+bedTop+5+10])
          cube([324,324,5]);

      if (details >= 4)
      {
        // bed on bottom
        bedX2020(bedBottom);
        mirror([0,1,0]) bedX2020(bedBottom);
        Zsetup(bedBottom);
        mirror([1,0,0]) Zsetup(bedBottom);
        translate([bedXoffset,0,0]) 
        {
          bedHolders(bedBottom);
          mirror([1,0,0]) bedHolders(bedBottom);
        }
        color("silver", 0.4) 
          translate([324/-2+bedXoffset,324/-2,frameOffsetZ-110+bedBottom+5+10])
            cube([324,324,5]);
      }
    }
  }
  if (details >= 3)
  {
    color("black", 0.5) translate([bandAxisX,-410/2+42.3/2+0.01,xyMotor1PosZ-0.01]) nema17(47, axisL=25);
    color("black", 0.5) translate([-bandAxisX,-410/2+42.3/2+0.01,xyMotor2PosZ-0.01]) nema17(47, axisL=25);
    frame();
  }
}
$fn=128;
include <Zsetup.scad>
size = 240;

!color("purple",0.8) 
rotate([180,0,0]) 
render()  
top3in1(pitch = size/2 - 12, bearingPlus=5, topHole = false);

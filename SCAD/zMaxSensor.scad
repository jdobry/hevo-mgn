off = [-15-2-2, -1+14, 7];
screw = 3;  // 5 or 3

M3hole = 3.4;
M3head = 6.2;
M3thread = 2.5; // screw directly to plastics

M5hole = 5.5;
M5head = 11.5;

$fn = $preview ? 0 : 64;

module zMaxFlag()
{
  dd = M5head+3;
  difference()
  {
    union()
    {
      translate([-2-1,0,-5]) cube([2,9,10]);
      translate([-20-1-1,0,-5]) cube([20,2,10]);
      hull()
      {
        translate([-20-1-dd/2,0,-5])
          rotate([-90,0,0])
            cylinder(d=dd,h=5);
        translate([-20-1-dd/2,0,+5])
          rotate([-90,0,0])
            cylinder(d=dd,h=5);
      }
    }
    hull()
    {
      translate([-20-1-dd/2,-1,-5])
        rotate([-90,0,0])
          cylinder(d=M5hole,h=7);
      translate([-20-1-dd/2,-1,+5])
        rotate([-90,0,0])
          cylinder(d=M5hole,h=7);
    }

  }
}

module zMaxHouse()
{
  difference()
  {
    union()
    {
      hull()
      {
        translate([28/-2,-10,0])
          cube([28,28,2]);
        translate([18/-2,-10+3,13])
          cube([18,28-3,2]);
      }
      translate([-35,3,0])
        cube([35,15,15]);
    }
    translate(off+[9.5,0,0])
      rotate([-90,0,0])
        cylinder(d=M3head,h=30);
    translate(off+[-9.5,0,0])
      rotate([-90,0,0])
        cylinder(d=M3head,h=30);
    translate(off+[9.5,-10,0])
      rotate([-90,0,0])
        cylinder(d=M3thread,h=30);
    translate(off+[-9.5,-10,0])
      rotate([-90,0,0])
        cylinder(d=M3thread,h=30);
    translate(off+[14.5,-1.5,-30+5.5])
      cube([7,10,30]);
    translate(off+[26/-2,-3,-30+5.5])
      cube([26,5,30-2]);
    translate(off+[12.4/-2,-12,-30+5.5])
      cube([12.4,12,30-2]);
    translate(off+[3/-2,-20-3,-30+5.5])
      cube([3,20,50]);
    translate(off+[28/-2,-2,-30+5.5])
      cube([28+8,5.5,30]);
      
    if (screw == 5)
    {
      translate([0,0,5])
          cylinder(d=M5head,h=30);
      translate([0,0,5-0.2])
        rotate([180,0,0])
          cylinder(d=M5hole,h=30);
    }
    else
    {
      translate([0,0,12])
          cylinder(d=M3head,h=30);
      translate([0,0,12-0.2])
        rotate([180,0,0])
          cylinder(d=M3hole,h=30);
    }
      
        
    translate(off+[14.5,-1.5,0])
      rotate([((screw == 5) ? 30 : 45), 0, 0])
        translate([0,0,-2])
          cylinder(d=2.05,h=30);

  }
}

if ($preview)
  color("gray", 0.6) 
    translate(off)
      rotate ([90,0,0]) 
        import("Endstop_Opto_TCST2103.stl");


if ($preview)
{
  color("lightgreen", 0.6) render()  zMaxHouse();
  color("purple", 0.6) translate([-(15+2),0,10+2]) render() zMaxFlag();
}
else
{
  translate([0,0,15]) rotate ([0,180,0]) render()  zMaxHouse();
  translate([0,-15,0]) rotate ([90,0,180]) zMaxFlag();
}

%color("silver", 0.4) translate ([-15,-50,-30-0.01]) cube([30,100,30]);
%color("silver", 0.4) translate ([-100-15-2,-20-0.01,2-0.01]) cube([100,20,20]);
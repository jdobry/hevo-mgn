include <HEVO_mgn_15_12.scad>

//$fn = 128;
//M3hole = 3.4;

//$fn = 64;

jetInner = 17.5; //15.5;
jethull = 21;
jetEx = 0; //3;
show = false;
$test = 0; //true;
fanWall2 = 1;
rExt = 12;
//rAssymetry = 7;
//nStreams = 9;
nStreams = 13;
nStreamsStep = 18;
nStreamsStart = 180+75;
offAxis = 4;

//tr = [-16.5,-11,-13.601];
tr = [-16.5,-11,-13.001];
connTr = -motor2absTransform - [0,0,-28] -tr;

//module streams(hX)
//{
//  for (i = [0:nStreams]) // paprsky vzduchu
//  {
//    rotate([0,0,i*26.45-19])
//      translate([-offAxis/* *(nStreams-1-i)/(nStreams-1) */-hX/2,-41,-5])
//        cube([hX,40,25]);
//  }
//}  
module streams(hX)
{
  for (i = [0:nStreams]) // paprsky vzduchu
  {
    rotate([0,0,nStreamsStart+i*nStreamsStep])
      translate([-offAxis/* *(nStreams-1-i)/(nStreams-1) */-hX/2,-41,-5])
        cube([hX,40,25]);
  }
}  

module streamsVisualization()
{
  intersection()
  {
    difference()
    {
      streams(0.5);
      translate([0,0,0.7-4])
        cylinder(r1=1, r2=1+62 ,h=20);
    }
    translate([0,0,0.7-4-0.5])
      cylinder(r1=1, r2=1+62 ,h=20);
    translate([0,0,-5])
      cylinder(r=jetInner+8 ,h=20);
  }
}
module jetsInner(m)
{
  hX = 2;
  asymetry = 0; //0.4;
  cyclone = 15;
  t2 = 1.8;
  
  intersection()
  {
    difference()
    {
      union()
      {
        streams(hX);
        translate([0,0,-1]) difference() // vzduchovy kanal v kruhu
        {
          hull() // zakladni tvar vzduchoveho hunelu
          {
            cylinder(r=jetInner+5+rExt+10, h=20);
            *translate([-jethull,0,0]) cylinder(r=jetInner+5+rExt+10-rAssymetry, h=20);
          }
          hull() // vnitrni krouzek je o 5mm mensi nez dira
          {
            translate([0,0,-1]) cylinder(r=jetInner+4.99, ,h=22);
            *translate([-jethull,0,-1]) cylinder(r=jetInner+4.99-rAssymetry, ,h=22);
          }
        }
      }
      intersection() // strop tunylku
      {
        hull()
        {
          translate([0,asymetry,t2])
            cylinder(r1=jetInner-asymetry, r2=jetInner+50 ,h=20+20);
          *translate([-jethull,asymetry,t2])
            cylinder(r1=jetInner-asymetry-rAssymetry, r2=jetInner+50-rAssymetry ,h=20+20);
        }
        hull()
        {
          translate([0,0,-1]) cylinder(r=jetInner+5, ,h=22);
          *translate([-jethull,0,-1]) cylinder(r=jetInner+5-rAssymetry, ,h=22);
        }
      }
      hull() // stop dutiny kruhu
      {
        translate([0,jetEx,m[2]-fanWall])
          cylinder(r=jetInner+rExt+1+jetEx+30,h=50);
        *translate([-jethull,jetEx,m[2]-fanWall])
          cylinder(r=jetInner+rExt+1+jetEx-rAssymetry,h=50);
      }

      // stena zleva
      *translate([-50-10-5/2-1+fanWall,-50,-5])
        cube([50,100,25]);
      
  /*      rotate([0,0,n*28])
        translate([-offAxis+hX/2,-50,-5])
          cube([40,50-10,25]);
  */
      *translate(m+[0,+20,-10-4])
        cylinder(d=M3head+2, h=4+1);

    }
    union() // spodek tunylku a spodek dutiny kruhu
    {
      hull() // spodek tunylku a spodek dutiny kruhu, sikma cast
      {
        translate([0,0,0])
          cylinder(r1=jetInner, r2=jetInner+62 ,h=20);
        *translate([-jethull,0,0])
          cylinder(r1=jetInner-rAssymetry, r2=jetInner+62-rAssymetry ,h=20);
      }
      hull() // spodek tunylku a spodek dutiny kruhu, sikma cast
      {
        translate([0,0,0.4 + fanWall])
          cylinder(r=jetInner+62 ,h=20);
        *translate([-jethull,0,0.4 + fanWall])
          cylinder(r=jetInner+62-rAssymetry ,h=20);
      }
    }
    difference()
    {
      hull() // vnejsi stena kruhu
      {
        translate([0,jetEx,-2])
          cylinder(r=jetInner+rExt+jetEx-fanWall2,h=m[2]+2+2);
        translate([(35+4-2*fanWall)/-2,jetEx+35+0.02,-fanWall+0.4])
              cube([35+4-2*fanWall, 0.01, m[2]+2*fanWall-0.4])
        *translate([-jethull,jetEx,-2])
          cylinder(r=jetInner+rExt+jetEx-fanWall2-rAssymetry,h=m[2]+2+2);
      }
      translate([(28+2*fanWall2)/-2,0,-10])
        cube([28+2*fanWall2,22+fanWall2,40]);
    }
  }
  // oriznuti zleva vnejsi
  *translate([-50-10-5/2-1,-50,-5])
    cube([50,200,80]);
  /*
  rotate([0,0,n*28])
    translate([-offAxis+hX/2+1,-50,-5])
      cube([40,50-3,25]);
  */
  hull() // spodni dira kruhu
  {
    translate([0,0,-5]) cylinder(r=jetInner,h=20);
    *translate([-jethull,0,-5]) cylinder(r=jetInner-rAssymetry,h=20);
  }
  intersection() // vybrani na kosku, dole sikme
  {
    hull()
    {
      translate([0,asymetry,t2+fanWall2*1.3])
        cylinder(r1=jetInner-asymetry, r2=jetInner+50, h=20+20);
      *translate([-jethull,asymetry,t2+fanWall2*1.3])
        cylinder(r1=jetInner-asymetry-rAssymetry, r2=jetInner+50-rAssymetry, h=20+20);
    }
    hull()
    {
      translate([0,0,-1]) cylinder(r=jetInner+5-fanWall2, h=22);
      *translate([-jethull,0,-1]) cylinder(r=jetInner+5-fanWall2-rAssymetry, h=22);
    }
  }
  // misto na kostku
  translate([28/-2,0,-2])
    cube([28,22,30]);
}

module jets(m)
{
  difference()
  {
    union()
    {
      union()
      {
        intersection()
        {
          hull()
          {
            translate([0,jetEx,-2])
              cylinder(r=jetInner+rExt+jetEx-1,h=m[2]+2);
            *translate([-jethull,jetEx,-2])
              cylinder(r=jetInner+rExt+jetEx-1-rAssymetry,h=m[2]+2);
          }
          hull()
          {
            translate([0,0,-1])
              cylinder(r1=jetInner, r2=jetInner+62, h=20);
            *translate([-jethull,0,-1])
              cylinder(r1=jetInner-rAssymetry, r2=jetInner+62-rAssymetry, h=20);
          }
        }
        hull()
        {
          translate([0,jetEx,0.4])
            cylinder(r=jetInner+rExt+jetEx,h=m[2]-0.4);
          translate([(35+4)/-2,jetEx+35,0.4])
            cube([35+4, 0.01, m[2]-0.4])
          *translate([-jethull,jetEx,0.4])
            cylinder(r=jetInner+rExt+jetEx-rAssymetry,h=m[2]-0.4);
        }

      }

      *hull()
      {
        translate(m+[0,+20,-10])
          cylinder(d=9,10);
        translate(m+[0-8,+20-8,-10])
          cylinder(d=22,10);
      }
      *#translate(m+[0,-20,0/*-10*/])
        cylinder(d=8,0.01); //10);
    }
    //jetCut();
    *translate(m+[0,+20,-20])
      cylinder(d=M3hole, h=30);
    *translate(m+[0,+20,-10-4])
      cylinder(d=M3head, h=4);
    
    *#translate(m+[0,-20,-20])
      cylinder(d=M3hole, h=30);
    
  }
}

module hotendMockup(rot = 0)
{
  translate([0,0,22])
  rotate([0, 180, (BMG_Aero) ? 180 : 0])
  { 
    cylinder(d=6,h=22);
    translate([0,0,22-3+0.01]) cylinder(d=7*(sin(30)+1),h=3, $fn=6);
    translate([0,0,22]) cylinder(d1=4, d2=1 ,h=2.02);
    //rotate([0,0,rot]) translate([-4.5,-8,6]) cube([20,16,11.5]);
    rotate([0,0,rot]) translate([-4.5-1.5,-8-1.5,6]) cube([20+3,16+3,11.5+4]);
  }
}

mmm = 4.1;
fd_a = 
[
  //   x     y       z      sy    sz   angle, support
  [  mmm,  -24.5,  -24.5,   30,   9.8,   0,  0], // spodni konec
  [  mmm,  -15  ,  -24.5,   25,  10.1,   0,  1.0],
  [  mmm,    0  ,  -23  ,   22,  12  ,  14,  1.2],
  [  mmm,   28.5,   -2  ,   24,  15  ,  80,  1.2],
  [  mmm,   31.2,    9  ,   24,  15.7,  90,  0.8],
  [  mmm,   31.1, 11.5  ,   24,  15.6,  90,  0], // horni konec
];

fdI_c = gen_dat_I(nSpline(fd_a,128),0);
fdO_c = gen_dat_O(nSpline(fd_a,128),fanWall);
fdX_c = gen_dat_s(nSpline(fd_a,128));

module fanductO(len,topL,offsetX,offsetY)
{
  difference()
  {
    union()
    {
      *hull() // back() connection
      {
        translate(fanscrew_1_abs-Cdiff) rotate([0,-90,0]) cylinder(d=10,h=4.1+2.9-Cdiff[0]);
        translate(fanscrew_2_abs-Cdiff) rotate([0,-90,0]) cylinder(d=10,h=4.1+2.9-Cdiff[0]);
      }
      *hull() // back() connection
      {
        translate(fanscrew_1_abs-Cdiff) rotate([0,-90,0]) cylinder(d=10,h=4.1+2.9-Cdiff[0]);
        translate(fanscrew_1_abs+[0,-2,-9]-Cdiff) rotate([0,-90,0]) cylinder(d=6,h=4.1+2.9-Cdiff[0]);
      }
      *hull() // back() connection
      {
        translate(fanscrew_1_abs+[0,-2,-9]-Cdiff) rotate([0,-90,0]) cylinder(d=6,h=4.1+2.9-Cdiff[0]);
        //translate(fanscrew_1_abs+[17,0,-23]) rotate([0,-90,0]) cylinder(d=6,h=4+17);
        translate(fanscrew_1_abs+[16,-5,-28]-Cdiff) rotate([0,-90,0]) cylinder(d=4,h=1-Cdiff[0]);
        translate(fanscrew_1_abs+[0,-5,-25]-Cdiff) rotate([0,-90,0]) cylinder(d=1,h=4.1+2.9-Cdiff[0]);
      }
      *hull() // back() connection
      {
        translate(fanscrew_2_abs+[-2.9,0,0]-[0,Cdiff[1],Cdiff[2]]) rotate([0,-90,0]) cylinder(d=10,h=4.1);
        translate(fanscrew_2_abs+[-2.9,10,-6]-[0,Cdiff[1],Cdiff[2]]) rotate([0,-90,0]) cylinder(d=10,h=4.1);
      }
      translate(-connTr) jets([20-13-5,0,0] - tr);

      *translate([fanXoffset,fanYoffset,fanZoffset]) 
        rotate([90,0,180])
          {
            hull()
            {
              translate([-fanWall,-1,-15-fanWall]) cube([20.5+fanWall*2,fanWall+1,15.5+fanWall*2]);
              translate([-fanWall,-1,-15-fanWall]) cube([22+fanWall*2,fanWall,15.5+fanWall*2]);
            }
            //translate([-fanWall,-1,-15-fanWall]) cube([28+fanWall*2,fanWall,15.5+fanWall*2]);
          }
      *sweep(fdO_c);

    }
    *translate([-3+10/2,carryX/2-12+5,-carryZ/2-6-50]+[headXoffset-rodOffset,-35,15]) 
        cylinder(d=M3hole,h=51);
    *difference()
    {
      sweep(fdI_c); 
      translate([fanXoffset,fanYoffset,fanZoffset]) // spunt do ktereho se bude vrtat dira pro ventilator
        rotate([90,0,180])
          translate([-0.5,0,-15-1/2]) cube([20.5+4.5,1.5,15.5+1]);// fanConnector();
    }

    *translate([fanXoffset,fanYoffset,fanZoffset]-Cdiff) // vrtani diry pro ventilator
      rotate([90,0,180])
        translate([0,0,-15]) cube([20.5,5,15.5]);// fanConnector();
  }

  // screw
  *translate ([fanXoffset-28+7/2-fanWall,fanYoffset-20+1.5,fanZoffset-7/2]) rotate([-90,0,0])cylinder(d=7,h=20.5);
  *intersection()
  {
    render () sweep(fdX_c);
    render () sweep(fdO_c);
  }
}

module fanductI(len,topL,offsetX,offsetY)
{
  *translate ([fanXoffset-28+7/2-fanWall,fanYoffset-20+1,fanZoffset-7/2]) rotate([-90,0,0])cylinder(d=M3hole,h=21);
    
  // screws
  *translate(fanscrew_1_abs+[1,0,0]-Cdiff) rotate([0,-90,0]) cylinder(d=M3hole,h=4+1-.2);
  *translate(fanscrew_2_abs+[1,0,0]-Cdiff) rotate([0,-90,0]) cylinder(d=M3hole,h=4+1-.2);

  *hull() // back() connection screw heads
  {
    translate(fanscrew_1_abs + [-4,0,0] -Cdiff) rotate([0,-90,0]) cylinder(d=M3head,h=4.1+2.9-Cdiff[0]);
    translate(fanscrew_2_abs + [-4,0,0] -Cdiff) rotate([0,-90,0]) cylinder(d=M3head,h=4.1+2.9-Cdiff[0]);
  }
}

module completeFanDuct()
{
  difference()
  {
    m = [20-13-5,0,0] - tr;
    render() translate(connTr) 
      fanductO(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    render() translate(connTr) 
      fanductI(topL=-15+16+mgnXposZ,offsetX=headXoffset-rodOffset,offsetY=-35-mgnPlusY);
    render() jetsInner(m);
    *translate(m+[-10.4/2,20-13/2-3,0]) // cisteni pripojeni na front()
      cube([10.4,16,10]);

    if ($test)
    {
      translate([-50-10-1,-50,5])
        cube([50,100,30]);
      translate([25,-50,-5])
        cube([50,100,30]);
    }
  }
}

//render() jetsInner([20-13-5,0,0] - tr);

if ($preview)
{
  color ("gold", 0.92)
    render()
      completeFanDuct(); //jets([20-13-5,0,0] - tr);

  color ("blue",0.7) 
    streamsVisualization();

  color ("red",0.5) 
    translate([0,0,-20-2])
      cylinder(d1=5,d2=0.5,h=20);

  color ("silver",0.7) 
   //titan();
    hotendMockup(-90);
}
else
{
  rotate([0,-90,0])
    completeFanDuct($fn=128, $test = false);
}
  
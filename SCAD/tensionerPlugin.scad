
$fn=64;
M3hole = 3.4;
zs = 6.5;

difference()
{
  hull()
  {
    translate([0,0,-zs/2]) cylinder(d=6, h=zs);
    translate([-3.5,-3,-zs/2]) cube([0.1,6,zs]);
  }
  rotate([0,-90,0]) cylinder(d=M3hole,h=20);
}
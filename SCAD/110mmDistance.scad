

$fn =64;

difference()
{
  union()
  {
    translate([-25,-10,0]) cube([110+2*25,20,5]);
    *translate([0,-10,0]) cube([110,20,10+5]);
    translate([0,-10,0]) cube([10,20,25]);
    *translate([100,-10,0]) cube([10,20,25]);
    hull()
    {
      translate([0,-10,0]) cube([10,20,10+5]);
      translate([110-12,-10,0]) cube([1,20,10]);
    }
    translate([110-12,-10,0]) cube([12,20,10]);
    
  }
  translate([10,-6,2]) cube([90+5,12,10+4]);
  translate([-1,0,5+15]) rotate([0,90,0]) cylinder(d=5.5,h=150);
  translate([-15,0,-1]) cylinder(d=5.5,h=150);
  translate([110+15,0,-1]) cylinder(d=5.5,h=150);
}
